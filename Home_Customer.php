<?php
ob_start();
session_start();
include 'Connection.php';
if (isset($_SESSION['suname']) and $_SESSION['spass']) {
    
} else {
    // header('Location: Login1.php');
}
?>

<html lang="en">
    <head>
        <title>Home</title>
        <?php include('Head.php') ?>
        <link rel="stylesheet" type="text/css" href="slick.css">
        <link rel="stylesheet" type="text/css" href="slick-theme.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">

    </head>
    <!--  text-transform: uppercase;-->
    <style>
        .slick-prev {
            left: -30px;
        }
        .slick-next {
            right: -30px;
        }
        h6.gt{
            text-align: center;


            position: relative;
        }

        h6
        {
            color: #000000;
            font-size: 20px;

            font-weight: 400;
            display: block;
            padding-bottom: 10px;
        }

        .before{content:'';
                background:url(home_line.png) no-repeat center;
                position:absolute;left:0;
                right:0;bottom:0;
                margin-bottom:-10px;
                width:100%;
                height:13px;
        }
    </style>
    <body class="animsition">

        <header class="header-v3">
            <?php include('Header.php') ?>
        </header>
        <?php include('Cart.php') ?>
        <section class="section-slide">
            <?php include('Slider.php') ?>
        </section>

        <!--Start Header -->

        <h6 class="gt">
            Popular Collection
            <div class="before">
            </div>
        </h6>


        <!-- End Header -- >
        
        
        
        <!-- Banner -->
        <div class="sec-banner bg0 p-t-95 p-b-55">
            <div class="container">
                <div class="row">

                    <!--
                    <div class="col-md-6 p-b-30 m-lr-auto">
                        
                        <div class="block1 wrap-pic-w">
                            <img src="images/banner-04.jpg" alt="IMG-BANNER">

                            <a href="product.html" class="block1-txt ab-t-l s-full flex-col-l-sb p-lr-38 p-tb-34 trans-03 respon3">
                                <div class="block1-txt-child1 flex-col-l">
                                    <span class="block1-name ltext-102 trans-04 p-b-8">
                                        Women
                                    </span>

                                    <span class="block1-info stext-102 trans-04">

                                    </span>
                                </div>

                                <div class="block1-txt-child2 p-b-4 trans-05">
                                    <div class="block1-link stext-101 cl0 trans-09">
                                        Shop Now
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="col-md-6 p-b-30 m-lr-auto">
                        
                        <div class="block1 wrap-pic-w">
                            <img src="images/banner-05.jpg" alt="IMG-BANNER">

                            <a href="product.html" class="block1-txt ab-t-l s-full flex-col-l-sb p-lr-38 p-tb-34 trans-03 respon3">
                                <div class="block1-txt-child1 flex-col-l">
                                    <span class="block1-name ltext-102 trans-04 p-b-8">
                                        Men 
                                    </span>

                                    <span class="block1-info stext-102 trans-04">

                                    </span>
                                </div>

                                <div class="block1-txt-child2 p-b-4 trans-05">
                                    <div class="block1-link stext-101 cl0 trans-09">
                                        Shop Now
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>-->

                    <!-- Start Slider -->


                    <div>

                        <div  id="s2" class="s12" style="height: 320px;width:1100px;margin: auto;">
                            <?php
                            include './Connection.php';
                            // $conn = new mysqli('Localhost', 'root', '', 'jewellary1');
                            $sql = "select * from tbl_pcategory";
                            $res = mysqli_query($conn, $sql);

                            if (mysqli_num_rows($res)) {
                                //echo "Data Found";

                                while ($row = mysqli_fetch_assoc($res)) {
                                    // echo $row['category_id'];
                                    ?>    
                                    <div>
                                        <div class="col-md-6 col-lg-12 p-b-30 m-lr-auto">



                                            <div class="block1 wrap-pic-w">
                                                <img src="<?php echo "admin/" . $row['category_image'] ?>" alt="IMG-BANNER">

                                                <a href="view_product.php?vid=<?php echo $row['name'] ?>" class="block1-txt ab-t-l s-full flex-col-l-sb p-lr-38 p-tb-34 trans-03 respon3">
                                                    <div class="block1-txt-child1 flex-col-l">
                                                        <span class="block1-name ltext-102 trans-04 p-b-8">
                                                            <?php echo ucwords($row['name']) ?>
                                                        </span>

                                                        <span class="block1-info stext-102 trans-04">

                                                        </span>
                                                    </div>

                                                    <div class="block1-txt-child2 p-b-4 trans-05">
                                                        <div class="block1-link stext-101 cl0 trans-09">
                                                            Shop Now
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>


                                        </div>
                                    </div>


                                    <?php
                                }
                            } else {
                                echo "Record Not Found";
                            }
                            ?>



                            <!-- <div>
                                 <div class="col-md-6 col-lg-12 p-b-30 m-lr-auto">
 
                                     <div class="block1 wrap-pic-w">
                                         <img src="images/banner-08.jpg" alt="IMG-BANNER">
 
                                         <a href="product.html" class="block1-txt ab-t-l s-full flex-col-l-sb p-lr-38 p-tb-34 trans-03 respon3">
                                             <div class="block1-txt-child1 flex-col-l">
                                                 <span class="block1-name ltext-102 trans-04 p-b-8">
                                                     Pendants
                                                 </span>
 
                                                 <span class="block1-info stext-102 trans-04">
 
                                                 </span>
                                             </div>
 
                                             <div class="block1-txt-child2 p-b-4 trans-05">
                                                 <div class="block1-link stext-101 cl0 trans-09">
                                                     Shop Now
                                                 </div>
                                             </div>
                                         </a>
                                     </div>
                                 </div>
                             </div>
                             <div>
                                 <div class="col-md-6 col-lg-12 p-b-30 m-lr-auto">
 
                                     <div class="block1 wrap-pic-w">
                                         <img src="images/banner-07.jpg" alt="IMG-BANNER">
 
                                         <a href="product.html" class="block1-txt ab-t-l s-full flex-col-l-sb p-lr-38 p-tb-34 trans-03 respon3">
                                             <div class="block1-txt-child1 flex-col-l">
                                                 <span class="block1-name ltext-102 trans-04 p-b-8">
                                                     Rings
                                                 </span>
 
                                                 <span class="block1-info stext-102 trans-04">
 
                                                 </span>
                                             </div>
 
                                             <div class="block1-txt-child2 p-b-4 trans-05">
                                                 <div class="block1-link stext-101 cl0 trans-09">
                                                     Shop Now
                                                 </div>
                                             </div>
                                         </a>
                                     </div>
                                 </div>
                             </div>
                             <div>
                                 <div class="col-md-6 col-lg-12 p-b-30 m-lr-auto">
 
                                     <div class="block1 wrap-pic-w">
                                         <img src="images/banner-08.jpg" alt="IMG-BANNER">
 
                                         <a href="product.html" class="block1-txt ab-t-l s-full flex-col-l-sb p-lr-38 p-tb-34 trans-03 respon3">
                                             <div class="block1-txt-child1 flex-col-l">
                                                 <span class="block1-name ltext-102 trans-04 p-b-8">
                                                     Pendants
                                                 </span>
 
                                                 <span class="block1-info stext-102 trans-04">
 
                                                 </span>
                                             </div>
 
                                             <div class="block1-txt-child2 p-b-4 trans-05">
                                                 <div class="block1-link stext-101 cl0 trans-09">
                                                     Shop Now
                                                 </div>
                                             </div>
                                         </a>
                                     </div>
                                 </div>
                             </div>-->

                        </div>
                    </div>


                    <!-- End Slider -->








                    <!--                    
                                        <div class="col-md-6 col-lg-4 p-b-30 m-lr-auto">
                                            <div class="block1 wrap-pic-w">
                                                <img src="images/banner-07.jpg" alt="IMG-BANNER">
                    
                                                <a href="product.html" class="block1-txt ab-t-l s-full flex-col-l-sb p-lr-38 p-tb-34 trans-03 respon3">
                                                    <div class="block1-txt-child1 flex-col-l">
                                                        <span class="block1-name ltext-102 trans-04 p-b-8">
                                                            Rings
                                                        </span>
                    
                                                    </div>
                    
                                                    <div class="block1-txt-child2 p-b-4 trans-05">
                                                        <div class="block1-link stext-101 cl0 trans-09">
                                                            Shop Now
                                                        </div>
                                                    </div>
                                                </a>
                                            </div> 
                    
                                        </div>
                                        <div class="col-md-6 col-lg-4 p-b-30 m-lr-auto">
                    
                                            <div class="block1 wrap-pic-w">
                                                <img src="images/banner-08.jpg" alt="IMG-BANNER">
                    
                                                <a href="product.html" class="block1-txt ab-t-l s-full flex-col-l-sb p-lr-38 p-tb-34 trans-03 respon3">
                                                    <div class="block1-txt-child1 flex-col-l">
                                                        <span class="block1-name ltext-102 trans-04 p-b-8">
                                                            Pendants
                                                        </span>
                    
                                                        <span class="block1-info stext-102 trans-04">
                    
                                                        </span>
                                                    </div>
                    
                                                    <div class="block1-txt-child2 p-b-4 trans-05">
                                                        <div class="block1-link stext-101 cl0 trans-09">
                                                            Shop Now
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                    
                                        <div class="col-md-6 col-lg-4 p-b-30 m-lr-auto">
                    
                                            <div class="block1 wrap-pic-w">
                                                <img src="images/banner-09.jpg" alt="IMG-BANNER">
                    
                                                <a href="product.html" class="block1-txt ab-t-l s-full flex-col-l-sb p-lr-38 p-tb-34 trans-03 respon3">
                                                    <div class="block1-txt-child1 flex-col-l">
                                                        <span class="block1-name ltext-102 trans-04 p-b-8">
                                                            Bangles
                                                        </span>
                    
                                                        <span class="block1-info stext-102 trans-04">
                    
                                                        </span>
                                                    </div>
                    
                                                    <div class="block1-txt-child2 p-b-4 trans-05">
                                                        <div class="block1-link stext-101 cl0 trans-09">
                                                            Shop Now
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                    -->
                </div>
            </div>
        </div>


        <!-- Product -->
        <section class="bg0 p-t-23 p-b-130">
            <div class="container">
                <div class="p-b-10">
                    <h3 class="ltext-103 cl5">
                        Product Overview
                    </h3>
                </div>
                <form action="#" method="post">
                    <div class="flex-w flex-sb-m p-b-52">
                        <div class="flex-w flex-l-m filter-tope-group m-tb-10">
                            <a href="Home_Customer.php" class="stext-106 cl6 hov1 bor3 trans-04 m-r-32 m-tb-5 how-active1" data-filter="*">
                                All Products
                            </a>

                            <?php
                            //$conn = new mysqli('Localhost', 'root', '', 'jewellary1');
                            $sql2 = "select * from tbl_pcategory";
                            $res2 = mysqli_query($conn, $sql2);

                            if (mysqli_num_rows($res2)) {
                                while ($row2 = mysqli_fetch_assoc($res2)) {
                                    ?>
                                    <a href="view_product.php?vid=<?php echo $row2['name'] ?>"  class="stext-106 cl6 hov1 bor3 trans-04 m-r-32 m-tb-5" >
                                            <?php echo ucwords($row2['name']) ?>
                                        </a>


                                    <?php
                                }
                            } else {
                                echo "No Category Exist!!";
                            }
                            ?>


                        </div>

                        <div class="flex-w flex-c-m m-tb-10">
                            <div class="flex-c-m stext-106 cl6 size-104 bor4 pointer hov-btn3 trans-04 m-r-8 m-tb-4 js-show-filter">
                                <i class="icon-filter cl2 m-r-6 fs-15 trans-04 zmdi zmdi-filter-list"></i>
                                <i class="icon-close-filter cl2 m-r-6 fs-15 trans-04 zmdi zmdi-close dis-none"></i>
                                Filter
                            </div>

                            <!--<div class="flex-c-m stext-106 cl6 size-105 bor4 pointer hov-btn3 trans-04 m-tb-4 js-show-search">
                                <i class="icon-search cl2 m-r-6 fs-15 trans-04 zmdi zmdi-search"></i>
                                <i class="icon-close-search cl2 m-r-6 fs-15 trans-04 zmdi zmdi-close dis-none"></i>
                                Search
                            </div>-->
                        </div>

                        <!-- Search product -->
                       <!-- <div class="dis-none panel-search w-full p-t-10 p-b-15">
                            <div class="bor8 dis-flex p-l-15">
                                <button class="size-113 flex-c-m fs-16 cl2 hov-cl1 trans-04">
                                    <i class="zmdi zmdi-search"></i>
                                </button>

                                <input class="mtext-107 cl2 size-114 plh2 p-r-15" type="text" name="search-product" placeholder="Search">
                            </div>	
                        </div>-->

                        <!-- Filter -->
                        <div class="dis-none panel-filter w-full p-t-10">
                            <div class="wrap-filter flex-w bg6 w-full p-lr-40 p-t-27 p-lr-15-sm">
                                <div class="filter-col1 p-r-15 p-b-27 " style="width: 20%">
                                    <div class="mtext-102 cl2 p-b-15">
                                        Metal
                                    </div>

                                    <div class="form-group">
                                        <div class="input-group">
                                            <select name="fmid"  class="form-control" >
                                                <option value="">All</option>
                                                <?php
                                                $sql2 = "select * from tbl_material_type";
                                                $res2 = mysqli_query($conn, $sql2);
                                                if (mysqli_num_rows($res2)) {
                                                    while ($row2 = mysqli_fetch_assoc($res2)) {
                                                        ?>
                                                        <option value="<?php echo $row2['type'] ?>"><?php echo ucwords($row2['type']); ?></option>
                                                        <?php
                                                    }
                                                } else {
                                                    echo "<p style='color:red'>Record Not Found!!</p>";
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="filter-col2 p-r-15 p-b-27" style="width:20%">
                                    <div class="mtext-102 cl2 p-b-15">
                                        Category
                                    </div>
                                    <div class="form-group" >
                                        <div class="input-group" style="height: 0%">
                                            <select name="fcid"  class="form-control" >
                                                <option value="">All</option>
                                                <?php
                                                $sql2 = "select * from tbl_pcategory";
                                                $res2 = mysqli_query($conn, $sql2);
                                                if (mysqli_num_rows($res2)) {
                                                    while ($row2 = mysqli_fetch_assoc($res2)) {
                                                        ?>
                                                        <option value="<?php echo $row2['name'] ?>"><?php echo ucwords($row2['name']); ?></option>
                                                        <?php
                                                    }
                                                } else {
                                                    echo "<p style='color:red'>Record Not Found!!</p>";
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>                                
                                </div>

                                <div class="filter-col2 p-r-15 p-b-27" style="width:20%">
                                    <div class="mtext-102 cl2 p-b-15">
                                        Purity
                                    </div>
                                    <div class="form-group" >
                                        <div class="input-group" style="height: 0%">
                                            <select name="fcid1"  class="form-control" >
                                                <option value="">All</option>
                                                <?php
                                                $sql2 = "select * from tbl_pcategory";
                                                $res2 = mysqli_query($conn, $sql2);
                                                if (mysqli_num_rows($res2)) {
                                                    while ($row2 = mysqli_fetch_assoc($res2)) {
                                                        ?>
                                                        <option value="<?php echo $row2['name'] ?>"><?php echo ucwords($row2['name']); ?></option>
                                                        <?php
                                                    }
                                                } else {
                                                    echo "<p style='color:red'>Record Not Found!!</p>";
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>                                
                                </div>

                                <div class="filter-col2 p-r-15 p-b-27" style="width:20%">
                                    <div class="mtext-102 cl2 p-b-15">
                                        Price
                                    </div>
                                    <div class="form-group" >
                                        <div class="input-group" style="height: 0%">
                                            <select name="fcid2"  class="form-control" >
                                                <option value="">All</option>
                                                <?php
                                                $sql2 = "select * from tbl_pcategory";
                                                $res2 = mysqli_query($conn, $sql2);
                                                if (mysqli_num_rows($res2)) {
                                                    while ($row2 = mysqli_fetch_assoc($res2)) {
                                                        ?>
                                                        <option value="<?php echo $row2['name'] ?>"><?php echo ucwords($row2['name']); ?></option>
                                                        <?php
                                                    }
                                                } else {
                                                    echo "<p style='color:red'>Record Not Found!!</p>";
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>                                
                                </div>
                                <div class="filter-col4 p-b-27" style="width: 20%">

                                    <div class="mtext-102 cl2 p-b-15">
                                        .
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            &nbsp;&nbsp;&nbsp;<input type="submit" name="filter" value="Filter" class="flex-c-m stext-101 cl0 size-121 bg3 bor1 hov-btn3 p-lr-15 trans-03 pointer" style="width: 70%;height:6% ;border-radius:8px;">
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </form>

                <!-- Product View -->

                <div class="row isotope-grid">


                    <?php
                    //include './Connection.php';
                    // $conn = new mysqli('Localhost', 'root', '', 'jewellary1');
                    // $sql = "select * from tbl_product where status=1";

                    $c = 0;

                    if (isset($_GET['fmid'])) {
                        $c = 1;
                        $fmid = $_GET['fmid'];
                    } else {
                        $fmid = "";
                    }
                    if (isset($_GET['fcid'])) {
                        $c = 1;
                        $fcid = $_GET['fcid'];
                    } else {
                        $fcid = "";
                    }

                    
                    if ($c == 1) {
                        $sql = "select tbl_product.*, tbl_pimage.* ,tbl_pcategory_type.cat_id,tbl_pcategory.name,tbl_material_type.type from tbl_product INNER JOIN tbl_pcategory_type on tbl_product.pct_id=tbl_pcategory_type.cat_type_id INNER join tbl_pcategory on tbl_pcategory_type.cat_id=tbl_pcategory.category_id INNER join tbl_material_type on tbl_pcategory_type.matel_id=tbl_material_type.mtype_id INNER JOIN tbl_pimage on  tbl_product.model_no=tbl_pimage.model_no where tbl_product.status=1 and tbl_pcategory.name LIKE '%$fcid%' and tbl_material_type.type like '%$fmid%' order by add_on DESC";
                    } else {
                        $sql = "select tbl_product.*, tbl_pimage.* ,tbl_pcategory_type.cat_id,tbl_pcategory.name,tbl_material_type.type from tbl_product INNER JOIN tbl_pcategory_type on tbl_product.pct_id=tbl_pcategory_type.cat_type_id INNER join tbl_pcategory on tbl_pcategory_type.cat_id=tbl_pcategory.category_id INNER join tbl_material_type on tbl_pcategory_type.matel_id=tbl_material_type.mtype_id INNER JOIN tbl_pimage on  tbl_product.model_no=tbl_pimage.model_no where tbl_product.status=1 order by add_on DESC";
                    }
                    $res = mysqli_query($conn, $sql);

                    if (mysqli_num_rows($res)) {
                        //echo "Data Found";

                        while ($row = mysqli_fetch_assoc($res)) {
                            // echo $row['category_id'];

                            $d1 = strtotime($row['add_on']);
                            $d2 = strtotime(date('Y-m-d'));

                            $ddd = $d2 - $d1;
                            $day = $ddd / (60 * 60 * 24);
                            ?>  



                            <div class="col-sm-6 col-md-4 col-lg-3 p-b-35 isotope-item women">
                                <!-- Block2 -->
                                <div class="block2">

                                    <?php
                                    if ($day > 3) {
                                        ?>

                                        <div class="block2-pic hov-img0 block1 wrap-pic-w" style="" >


                                        <?php } else { ?>
                                            <div class="block2-pic hov-img0 block1 wrap-pic-w label-new" style="" data-label="New" >

                                            <?php } ?>
                                            <img src="<?php echo "admin/" . $row['image'] ?>" alt="IMG-PRODUCT">

                                            <a href='#' class="block2-btn flex-c-m stext-103 cl2 size-102 bg3 bor2 hov-btn3 p-lr-15 trans-04" style="color: white" data-toggle="modal"  data-target="#<?php echo "v_" . $row['model_no'] ?>">


                                                Quick View
                                            </a>


                                            <div class="modal fade" id="<?php echo "v_" . $row['model_no'] ?>" role="dialog">
                                                <div class="modal-dialog modal-dialog-centered modal-lg">
                                                    <div class="modal-content">
                                                        <!-- <div class="modal-header">

                                                               <h4 class="modal-title">Modal Header</h4> 
                                                             <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                          </div>-->
                                                        <div class="modal-body" style="width: 100%;height:73%">

                                                            <button type="button" class="close" data-dismiss="modal" style="float: right;">&times;</button>
                                                            <p>Model No : 
                                                                <?php echo "<b>" . $row['model_no'] . "</b>"; ?>
                                                            </p>

                                                            <div class="row">
                                                                <div class="col-md-6 col-lg-7 p-b-30">
                                                                    <div class="p-l-25 p-r-30 p-lr-0-lg">
                                                                        <div class="wrap-slick3 flex-sb flex-w">
                                                                            <div class="wrap-slick3-dots"></div>
                                                                            <div class="wrap-slick3-arrows flex-sb-m flex-w"></div>

                                                                            <div class="slick3 gallery-lb " >
                                                                                <div class="item-slick3" data-thumb="<?php echo $row['image1'] ?>">
                                                                                    <div class="wrap-pic-w pos-relative ">
                                                                                        <img src="<?php echo $row['image1'] ?>" alt="IMG-PRODUCT">

                                                                                        <a class="flex-c-m size-108 how-pos1 bor0 fs-16 cl10 bg0 hov-btn3 trans-04" href="<?php echo $row['image1'] ?>">
                                                                                            <i class="fa fa-expand"></i>
                                                                                        </a>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="item-slick3" data-thumb="<?php echo $row['image2'] ?>">
                                                                                    <div class="wrap-pic-w pos-relative">
                                                                                        <img src="<?php echo $row['image2'] ?>" alt="IMG-PRODUCT">

                                                                                        <a class="flex-c-m size-108 how-pos1 bor0 fs-16 cl10 bg0 hov-btn3 trans-04" href="<?php echo $row['image2'] ?>">
                                                                                            <i class="fa fa-expand"></i>
                                                                                        </a>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="item-slick3" data-thumb="<?php echo $row['image3'] ?>">
                                                                                    <div class="wrap-pic-w pos-relative">
                                                                                        <img src="<?php echo $row['image3'] ?>" alt="IMG-PRODUCT">

                                                                                        <a class="flex-c-m size-108 how-pos1 bor0 fs-16 cl10 bg0 hov-btn3 trans-04" href="<?php echo $row['image3'] ?>">
                                                                                            <i class="fa fa-expand"></i>
                                                                                        </a>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="item-slick3" data-thumb="<?php echo $row['image4'] ?>">
                                                                                    <div class="wrap-pic-w pos-relative">
                                                                                        <img src="<?php echo $row['image4'] ?>" alt="IMG-PRODUCT">

                                                                                        <a class="flex-c-m size-108 how-pos1 bor0 fs-16 cl10 bg0 hov-btn3 trans-04" href="<?php echo $row['image4'] ?>">
                                                                                            <i class="fa fa-expand"></i>
                                                                                        </a>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-6 col-lg-5 p-b-30">
                                                                    <div class="p-r-50 p-t-5 p-lr-0-lg">
                                                                        <b><h4 class="mtext-104 cl2 js-name-detail p-b-14">
                                                                                <?php echo ucwords($row['type']) . " " . ucwords($row['name']) ?>
                                                                            </h4>
                                                                            <h4 class="mtext-104 cl2 js-name-detail p-b-14" ><?php echo "Weight : " . $row['weight'] . " gm"; ?>
                                                                            </h4>

                                                                        </b>

                                                                        <span class="mtext-106 cl2">
                                                                            Price : <i class="fa fa-rupee"></i>&nbsp;<?php echo $row['price'] ?>
                                                                        </span>

                                                                        <p class="stext-102 cl3 p-t-23">
                                                                            Nulla eget sem vitae eros pharetra viverra. Nam vitae luctus ligula. Mauris consequat ornare feugiat.
                                                                        </p>

                                                                        <!--  -->
                                                                        <div class="p-t-33">


                                                                            <div class="flex-w flex-r-m p-b-10">
                                                                                <div class="size-204 flex-w flex-m respon6-next">
                                                                                    <div class="wrap-num-product flex-w m-r-20 m-tb-10">
                                                                                        <div class="btn-num-product-down cl8 hov-btn3 trans-04 flex-c-m" onclick="minusquant(<?php echo $row['model_no'] ?>)">
                                                                                            <i class="fs-16 zmdi zmdi-minus" ></i>
                                                                                        </div>

                                                                                        <input class="mtext-104 cl3 txt-center num-product" type="number" name="num-product" value="1" id="quant_<?php echo $row['model_no'] ?>">

                                                                                        <div class="btn-num-product-up cl8 hov-btn3 trans-04 flex-c-m" onclick="plusquant(<?php echo $row['model_no'] ?>)">
                                                                                            <i class="fs-16 zmdi zmdi-plus" ></i>
                                                                                        </div>
                                                                                    </div>
                                                                                    <input type="hidden" name="hd" id="hd1_<?php echo $row['model_no'] ?>" value="<?php echo $row['quantity'] ?>">

                                                                                    <?php if (isset($_SESSION['suname'])) { ?>
                                                                                        <p id="sc1"></p>
                                                                                        <input type="button" name="cart" value="Add to Cart" onclick="myfunction('<?php echo $row['model_no'] ?>')" class="flex-c-m stext-101 cl0 size-101 bg1 bor1 hov-btn1 p-lr-15 trans-04 js-addcart-detail" data-dismiss='modal' >
                                                                                    <?php } else { ?>

                                                                                        <b><p style="color: red"> Please do Login then You Add The Product In Your Cart</p></b>
                                                                                    <?php } ?>
                                                                                </div>
                                                                            </div>	
                                                                        </div>

                                                                        <!--  -->

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>


                                        </div>

                                        <div class="block2-txt flex-w flex-t p-t-14">
                                            <div class="block2-txt-child1 flex-col-l ">
                                                <a href="product-detail.html" class="stext-104 cl4 hov-cl1 trans-04 js-name-b2 p-b-6">
                                                    <?php echo "<b>" . ucwords($row['type']) . "</b> " . ucwords($row['name']); ?>
                                                </a>

                                                <span class="stext-105 cl3">
                                                    <i class="fa fa-rupee"></i><?php echo $row['price'] ?>
                                                </span>
                                            </div>

                                            <div class="block2-txt-child2 flex-r p-t-3 ">
                                                <?php // echo "Mno:".$row['model_no']   ?>
                                                <a href="#" class="btn-addwish-b2 dis-block pos-relative js-addwish-b2">

                                                    <img class="icon-heart1 dis-block trans-04" src="images/icons/icon-heart-01.png" alt="ICON">
                                                    <img class="icon-heart2 dis-block trans-04 ab-t-l" src="images/icons/icon-heart-02.png" alt="ICON">
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--   <div class="col-sm-6 col-md-4 col-lg-3 p-b-35 isotope-item women">
                                       
                                       <div class="block2">
                                           <div class="block2-pic hov-img0">
                                               <img src="images/product-02.jpg" alt="IMG-PRODUCT">
               
                                               <a href="#" class="block2-btn flex-c-m stext-103 cl2 size-102 bg0 bor2 hov-btn1 p-lr-15 trans-04 js-show-modal1">
                                                   Quick View
                                               </a>
                                           </div>
               
                                           <div class="block2-txt flex-w flex-t p-t-14">
                                               <div class="block2-txt-child1 flex-col-l ">
                                                   <a href="product-detail.html" class="stext-104 cl4 hov-cl1 trans-04 js-name-b2 p-b-6">
                                                       Ring
                                                   </a>
               
                                                   <span class="stext-105 cl3">
                                                       <i class="fa fa-rupee"></i>35000.00
                                                   </span>
                                               </div>
               
                                               <div class="block2-txt-child2 flex-r p-t-3">
                                                   <a href="#" class="btn-addwish-b2 dis-block pos-relative js-addwish-b2">
                                                       <img class="icon-heart1 dis-block trans-04" src="images/icons/icon-heart-01.png" alt="ICON">
                                                       <img class="icon-heart2 dis-block trans-04 ab-t-l" src="images/icons/icon-heart-02.png" alt="ICON">
                                                   </a>
                                               </div>
                                           </div>
                                       </div>
                                   </div>
               
                                   <div class="col-sm-6 col-md-4 col-lg-3 p-b-35 isotope-item men">
                                       
                                       <div class="block2">
                                           <div class="block2-pic hov-img0">
                                               <img src="images/product-03.jpg" alt="IMG-PRODUCT">
               
                                               <a href="#" class="block2-btn flex-c-m stext-103 cl2 size-102 bg0 bor2 hov-btn1 p-lr-15 trans-04 js-show-modal1">
                                                   Quick View
                                               </a>
                                           </div>
               
                                           <div class="block2-txt flex-w flex-t p-t-14">
                                               <div class="block2-txt-child1 flex-col-l ">
                                                   <a href="product-detail.html" class="stext-104 cl4 hov-cl1 trans-04 js-name-b2 p-b-6">
                                                       Diamond Ring
                                                   </a>
               
                                                   <span class="stext-105 cl3">
                                                       <i class="fa fa-rupee"></i>20050.00
                                                   </span>
                                               </div>
               
                                               <div class="block2-txt-child2 flex-r p-t-3">
                                                   <a href="#" class="btn-addwish-b2 dis-block pos-relative js-addwish-b2">
                                                       <img class="icon-heart1 dis-block trans-04" src="images/icons/icon-heart-01.png" alt="ICON">
                                                       <img class="icon-heart2 dis-block trans-04 ab-t-l" src="images/icons/icon-heart-02.png" alt="ICON">
                                                   </a>
                                               </div>
                                           </div>
                                       </div>
                                   </div>
               
                                   <div class="col-sm-6 col-md-4 col-lg-3 p-b-35 isotope-item women">
                                       
                                       <div class="block2">
                                           <div class="block2-pic hov-img0">
                                               <img src="images/product-04.jpg" alt="IMG-PRODUCT">
               
                                               <a href="#" class="block2-btn flex-c-m stext-103 cl2 size-102 bg0 bor2 hov-btn1 p-lr-15 trans-04 js-show-modal1">
                                                   Quick View
                                               </a>
                                           </div>
               
                                           <div class="block2-txt flex-w flex-t p-t-14">
                                               <div class="block2-txt-child1 flex-col-l ">
                                                   <a href="product-detail.html" class="stext-104 cl4 hov-cl1 trans-04 js-name-b2 p-b-6">
                                                       Diamond Bangles
                                                   </a>
               
                                                   <span class="stext-105 cl3">
                                                       <i class="fa fa-rupee"></i>75000.00
                                                   </span>
                                               </div>
               
                                               <div class="block2-txt-child2 flex-r p-t-3">
                                                   <a href="#" class="btn-addwish-b2 dis-block pos-relative js-addwish-b2">
                                                       <img class="icon-heart1 dis-block trans-04" src="images/icons/icon-heart-01.png" alt="ICON">
                                                       <img class="icon-heart2 dis-block trans-04 ab-t-l" src="images/icons/icon-heart-02.png" alt="ICON">
                                                   </a>
                                               </div>
                                           </div>
                                       </div>
                                   </div>
               
                                   <div class="col-sm-6 col-md-4 col-lg-3 p-b-35 isotope-item women">
                                       
                                       <div class="block2">
                                           <div class="block2-pic hov-img0">
                                               <img src="images/product-05.jpg" alt="IMG-PRODUCT">
               
                                               <a href="#" class="block2-btn flex-c-m stext-103 cl2 size-102 bg0 bor2 hov-btn1 p-lr-15 trans-04 js-show-modal1">
                                                   Quick View
                                               </a>
                                           </div>
               
                                           <div class="block2-txt flex-w flex-t p-t-14">
                                               <div class="block2-txt-child1 flex-col-l ">
                                                   <a href="product-detail.html" class="stext-104 cl4 hov-cl1 trans-04 js-name-b2 p-b-6">
                                                       Platinum Ring
                                                   </a>
               
                                                   <span class="stext-105 cl3">
                                                       <i class="fa fa-rupee"></i>34000.00
                                                   </span>
                                               </div>
               
                                               <div class="block2-txt-child2 flex-r p-t-3">
                                                   <a href="#" class="btn-addwish-b2 dis-block pos-relative js-addwish-b2">
                                                       <img class="icon-heart1 dis-block trans-04" src="images/icons/icon-heart-01.png" alt="ICON">
                                                       <img class="icon-heart2 dis-block trans-04 ab-t-l" src="images/icons/icon-heart-02.png" alt="ICON">
                                                   </a>
                                               </div>
                                           </div>
                                       </div>
                                   </div>
               
                                   <div class="col-sm-6 col-md-4 col-lg-3 p-b-35 isotope-item watches">
                                       
                                       <div class="block2">
                                           <div class="block2-pic hov-img0">
                                               <img src="images/product-06.jpg" alt="IMG-PRODUCT">
               
                                               <a href="#" class="block2-btn flex-c-m stext-103 cl2 size-102 bg0 bor2 hov-btn1 p-lr-15 trans-04 js-show-modal1">
                                                   Quick View
                                               </a>
                                           </div>
               
                                           <div class="block2-txt flex-w flex-t p-t-14">
                                               <div class="block2-txt-child1 flex-col-l ">
                                                   <a href="product-detail.html" class="stext-104 cl4 hov-cl1 trans-04 js-name-b2 p-b-6">
                                                       Ear-rings
                                                   </a>
               
                                                   <span class="stext-105 cl3">
                                                       <i class="fa fa-rupee"></i>9300.00
                                                   </span>
                                               </div>
               
                                               <div class="block2-txt-child2 flex-r p-t-3">
                                                   <a href="#" class="btn-addwish-b2 dis-block pos-relative js-addwish-b2">
                                                       <img class="icon-heart1 dis-block trans-04" src="images/icons/icon-heart-01.png" alt="ICON">
                                                       <img class="icon-heart2 dis-block trans-04 ab-t-l" src="images/icons/icon-heart-02.png" alt="ICON">
                                                   </a>
                                               </div>
                                           </div>
                                       </div>
                                   </div>
               
                                   <div class="col-sm-6 col-md-4 col-lg-3 p-b-35 isotope-item women">
                                       
                                       <div class="block2">
                                           <div class="block2-pic hov-img0">
                                               <img src="images/product-07.jpg" alt="IMG-PRODUCT">
               
                                               <a href="#" class="block2-btn flex-c-m stext-103 cl2 size-102 bg0 bor2 hov-btn1 p-lr-15 trans-04 js-show-modal1">
                                                   Quick View
                                               </a>
                                           </div>
               
                                           <div class="block2-txt flex-w flex-t p-t-14">
                                               <div class="block2-txt-child1 flex-col-l ">
                                                   <a href="product-detail.html" class="stext-104 cl4 hov-cl1 trans-04 js-name-b2 p-b-6">
                                                       Bangles
                                                   </a>
               
                                                   <span class="stext-105 cl3">
                                                       <i class="fa fa-rupee"></i>5200.00
                                                   </span>
                                               </div>
               
                                               <div class="block2-txt-child2 flex-r p-t-3">
                                                   <a href="#" class="btn-addwish-b2 dis-block pos-relative js-addwish-b2">
                                                       <img class="icon-heart1 dis-block trans-04" src="images/icons/icon-heart-01.png" alt="ICON">
                                                       <img class="icon-heart2 dis-block trans-04 ab-t-l" src="images/icons/icon-heart-02.png" alt="ICON">
                                                   </a>
                                               </div>
                                           </div>
                                       </div>
                                   </div>
               
                                   <div class="col-sm-6 col-md-4 col-lg-3 p-b-35 isotope-item women">
                                       
                                       <div class="block2">
                                           <div class="block2-pic hov-img0">
                                               <img src="images/product-08.jpg" alt="IMG-PRODUCT">
               
                                               <a href="#" class="block2-btn flex-c-m stext-103 cl2 size-102 bg0 bor2 hov-btn1 p-lr-15 trans-04 js-show-modal1">
                                                   Quick View
                                               </a>
                                           </div>
               
                                           <div class="block2-txt flex-w flex-t p-t-14">
                                               <div class="block2-txt-child1 flex-col-l ">
                                                   <a href="product-detail.html" class="stext-104 cl4 hov-cl1 trans-04 js-name-b2 p-b-6">
                                                       Pandant
                                                   </a>
               
                                                   <span class="stext-105 cl3">
                                                       <i class="fa fa-rupee"></i>18000.00
                                                   </span>
                                               </div>
               
                                               <div class="block2-txt-child2 flex-r p-t-3">
                                                   <a href="#" class="btn-addwish-b2 dis-block pos-relative js-addwish-b2">
                                                       <img class="icon-heart1 dis-block trans-04" src="images/icons/icon-heart-01.png" alt="ICON">
                                                       <img class="icon-heart2 dis-block trans-04 ab-t-l" src="images/icons/icon-heart-02.png" alt="ICON">
                                                   </a>
                                               </div>
                                           </div>
                                       </div>
                                   </div>
               
                                   <div class="col-sm-6 col-md-4 col-lg-3 p-b-35 isotope-item shoes">
                                      
                                       <div class="block2">
                                           <div class="block2-pic hov-img0">
                                               <img src="images/product-09.png" alt="IMG-PRODUCT">
               
                                               <a href="#" class="block2-btn flex-c-m stext-103 cl2 size-102 bg0 bor2 hov-btn1 p-lr-15 trans-04 js-show-modal1">
                                                   Quick View
                                               </a>
                                           </div>
               
                                           <div class="block2-txt flex-w flex-t p-t-14">
                                               <div class="block2-txt-child1 flex-col-l ">
                                                   <a href="product-detail.html" class="stext-104 cl4 hov-cl1 trans-04 js-name-b2 p-b-6">
                                                       Pandant
                                                   </a>
               
                                                   <span class="stext-105 cl3">
                                                       <i class="fa fa-rupee"></i>7500.00
                                                   </span>
                                               </div>
               
                                               <div class="block2-txt-child2 flex-r p-t-3">
                                                   <a href="#" class="btn-addwish-b2 dis-block pos-relative js-addwish-b2">
                                                       <img class="icon-heart1 dis-block trans-04" src="images/icons/icon-heart-01.png" alt="ICON">
                                                       <img class="icon-heart2 dis-block trans-04 ab-t-l" src="images/icons/icon-heart-02.png" alt="ICON">
                                                   </a>
                                               </div>
                                           </div>
                                       </div>
                                   </div>
               
                                   <div class="col-sm-6 col-md-4 col-lg-3 p-b-35 isotope-item women">
                                       
                                       <div class="block2">
                                           <div class="block2-pic hov-img0">
                                               <img src="images/product-10.png" alt="IMG-PRODUCT">
               
                                               <a href="#" class="block2-btn flex-c-m stext-103 cl2 size-102 bg0 bor2 hov-btn1 p-lr-15 trans-04 js-show-modal1">
                                                   Quick View
                                               </a>
                                           </div>
               
                                           <div class="block2-txt flex-w flex-t p-t-14">
                                               <div class="block2-txt-child1 flex-col-l ">
                                                   <a href="product-detail.html" class="stext-104 cl4 hov-cl1 trans-04 js-name-b2 p-b-6">
                                                       Braclet
                                                   </a>
               
                                                   <span class="stext-105 cl3">
                                                       <i class="fa fa-rupee"></i>2500.00
                                                   </span>
                                               </div>
               
                                               <div class="block2-txt-child2 flex-r p-t-3">
                                                   <a href="#" class="btn-addwish-b2 dis-block pos-relative js-addwish-b2">
                                                       <img class="icon-heart1 dis-block trans-04" src="images/icons/icon-heart-01.png" alt="ICON">
                                                       <img class="icon-heart2 dis-block trans-04 ab-t-l" src="images/icons/icon-heart-02.png" alt="ICON">
                                                   </a>
                                               </div>
                                           </div>
                                       </div>
                                   </div>
               
                                   <div class="col-sm-6 col-md-4 col-lg-3 p-b-35 isotope-item men">
                                       
                                       <div class="block2">
                                           <div class="block2-pic hov-img0">
                                               <img src="images/product-11.png" alt="IMG-PRODUCT">
               
                                               <a href="#" class="block2-btn flex-c-m stext-103 cl2 size-102 bg0 bor2 hov-btn1 p-lr-15 trans-04 js-show-modal1">
                                                   Quick View
                                               </a>
                                           </div>
               
                                           <div class="block2-txt flex-w flex-t p-t-14">
                                               <div class="block2-txt-child1 flex-col-l ">
                                                   <a href="product-detail.html" class="stext-104 cl4 hov-cl1 trans-04 js-name-b2 p-b-6">
                                                       Ring
                                                   </a>
               
                                                   <span class="stext-105 cl3">
                                                       <i class="fa fa-rupee"></i>6300.00
                                                   </span>
                                               </div>
               
                                               <div class="block2-txt-child2 flex-r p-t-3">
                                                   <a href="#" class="btn-addwish-b2 dis-block pos-relative js-addwish-b2">
                                                       <img class="icon-heart1 dis-block trans-04" src="images/icons/icon-heart-01.png" alt="ICON">
                                                       <img class="icon-heart2 dis-block trans-04 ab-t-l" src="images/icons/icon-heart-02.png" alt="ICON">
                                                   </a>
                                               </div>
                                           </div>
                                       </div>
                                   </div>
               
                                   <div class="col-sm-6 col-md-4 col-lg-3 p-b-35 isotope-item men">
                                       
                                       <div class="block2">
                                           <div class="block2-pic hov-img0">
                                               <img src="images/product-12.jpg" alt="IMG-PRODUCT">
               
                                               <a href="#" class="block2-btn flex-c-m stext-103 cl2 size-102 bg0 bor2 hov-btn1 p-lr-15 trans-04 js-show-modal1">
                                                   Quick View
                                               </a>
                                           </div>
               
                                           <div class="block2-txt flex-w flex-t p-t-14">
                                               <div class="block2-txt-child1 flex-col-l ">
                                                   <a href="product-detail.html" class="stext-104 cl4 hov-cl1 trans-04 js-name-b2 p-b-6">
                                                       Diamond
                                                   </a>
               
                                                   <span class="stext-105 cl3">
                                                       <i class="fa fa-rupee"></i>63000.00
                                                   </span>
                                               </div>
               
                                               <div class="block2-txt-child2 flex-r p-t-3">
                                                   <a href="#" class="btn-addwish-b2 dis-block pos-relative js-addwish-b2">
                                                       <img class="icon-heart1 dis-block trans-04" src="images/icons/icon-heart-01.png" alt="ICON">
                                                       <img class="icon-heart2 dis-block trans-04 ab-t-l" src="images/icons/icon-heart-02.png" alt="ICON">
                                                   </a>
                                               </div>
                                           </div>
                                       </div>
                                   </div>
               
                                   <div class="col-sm-6 col-md-4 col-lg-3 p-b-35 isotope-item women">
                                       
                                       <div class="block2">
                                           <div class="block2-pic hov-img0">
                                               <img src="images/product-13.jpg" alt="IMG-PRODUCT">
               
                                               <a href="#" class="block2-btn flex-c-m stext-103 cl2 size-102 bg0 bor2 hov-btn1 p-lr-15 trans-04 js-show-modal1">
                                                   Quick View
                                               </a>
                                           </div>
               
                                           <div class="block2-txt flex-w flex-t p-t-14">
                                               <div class="block2-txt-child1 flex-col-l ">
                                                   <a href="product-detail.html" class="stext-104 cl4 hov-cl1 trans-04 js-name-b2 p-b-6">
                                                       Pandant
                                                   </a>
               
                                                   <span class="stext-105 cl3">
                                                       <i class="fa fa-rupee"></i>1800.00
                                                   </span>
                                               </div>
               
                                               <div class="block2-txt-child2 flex-r p-t-3">
                                                   <a href="#" class="btn-addwish-b2 dis-block pos-relative js-addwish-b2">
                                                       <img class="icon-heart1 dis-block trans-04" src="images/icons/icon-heart-01.png" alt="ICON">
                                                       <img class="icon-heart2 dis-block trans-04 ab-t-l" src="images/icons/icon-heart-02.png" alt="ICON">
                                                   </a>
                                               </div>
                                           </div>
                                       </div>
                                   </div>
               
                                   <div class="col-sm-6 col-md-4 col-lg-3 p-b-35 isotope-item women">
                                       
                                       <div class="block2">
                                           <div class="block2-pic hov-img0">
                                               <img src="images/product-14.jpg" alt="IMG-PRODUCT">
               
                                               <a href="#" class="block2-btn flex-c-m stext-103 cl2 size-102 bg0 bor2 hov-btn1 p-lr-15 trans-04 js-show-modal1">
                                                   Quick View
                                               </a>
                                           </div>
               
                                           <div class="block2-txt flex-w flex-t p-t-14">
                                               <div class="block2-txt-child1 flex-col-l ">
                                                   <a href="product-detail.html" class="stext-104 cl4 hov-cl1 trans-04 js-name-b2 p-b-6">
                                                       Ear-Rings
                                                   </a>
               
                                                   <span class="stext-105 cl3">
                                                       <i class="fa fa-rupee"></i>5400.00
                                                   </span>
                                               </div>
               
                                               <div class="block2-txt-child2 flex-r p-t-3">
                                                   <a href="#" class="btn-addwish-b2 dis-block pos-relative js-addwish-b2">
                                                       <img class="icon-heart1 dis-block trans-04" src="images/icons/icon-heart-01.png" alt="ICON">
                                                       <img class="icon-heart2 dis-block trans-04 ab-t-l" src="images/icons/icon-heart-02.png" alt="ICON">
                                                   </a>
                                               </div>
                                           </div>
                                       </div>
                                   </div>
               
                                   <div class="col-sm-6 col-md-4 col-lg-3 p-b-35 isotope-item watches">
                                       
                                       <div class="block2">
                                           <div class="block2-pic hov-img0">
                                               <img src="images/product-15.jpg" alt="IMG-PRODUCT">
               
                                               <a href="#" class="block2-btn flex-c-m stext-103 cl2 size-102 bg0 bor2 hov-btn1 p-lr-15 trans-04 js-show-modal1">
                                                   Quick View
                                               </a>
                                           </div>
               
                                           <div class="block2-txt flex-w flex-t p-t-14">
                                               <div class="block2-txt-child1 flex-col-l ">
                                                   <a href="product-detail.html" class="stext-104 cl4 hov-cl1 trans-04 js-name-b2 p-b-6">
                                                       Bangles
                                                   </a>
               
                                                   <span class="stext-105 cl3">
                                                       <i class="fa fa-rupee"></i>8600.00
                                                   </span>
                                               </div>
               
                                               <div class="block2-txt-child2 flex-r p-t-3">
                                                   <a href="#" class="btn-addwish-b2 dis-block pos-relative js-addwish-b2">
                                                       <img class="icon-heart1 dis-block trans-04" src="images/icons/icon-heart-01.png" alt="ICON">
                                                       <img class="icon-heart2 dis-block trans-04 ab-t-l" src="images/icons/icon-heart-02.png" alt="ICON">
                                                   </a>
                                               </div>
                                           </div>
                                       </div>
                                   </div>
               
                                   <div class="col-sm-6 col-md-4 col-lg-3 p-b-35 isotope-item women">
                                       
                                       <div class="block2" >
                                           <div class="block2-pic hov-img0" >
                                               <img src="images/product-16.jpg" alt="IMG-PRODUCT">
               
                                               <a href="#" class="block2-btn flex-c-m stext-103 cl2 size-102 bg0 bor2 hov-btn1 p-lr-15 trans-04 js-show-modal1">
                                                   Quick View
                                               </a>
                                           </div>
               
                                           <div class="block2-txt flex-w flex-t p-t-14">
                                               <div class="block2-txt-child1 flex-col-l ">
                                                   <a href="product-detail.html" class="stext-104 cl4 hov-cl1 trans-04 js-name-b2 p-b-6">
                                                       Ring
                                                   </a>
               
                                                   <span class="stext-105 cl3">
                                                       <i class="fa fa-rupee"></i>2900.00
                                                   </span>
                                               </div>
               
                                               <div class="block2-txt-child2 flex-r p-t-3">
                                                   <a href="#" class="btn-addwish-b2 dis-block pos-relative js-addwish-b2">
                                                       <img class="icon-heart1 dis-block trans-04" src="images/icons/icon-heart-01.png" alt="ICON">
                                                       <img class="icon-heart2 dis-block trans-04 ab-t-l" src="images/icons/icon-heart-02.png" alt="ICON">
                                                   </a>
                                               </div>
                                           </div>
                                       </div>
                                   </div> -->


                                <?php
                            }
                        } else {
                             echo "<b><p style='color:red'>Currently Product Not Exist!!</p></b>";
                        }
                        ?>


                    </div>

                    <!--End Product View -->

                    <!-- Pagination -->
                    <div class="flex-c-m flex-w w-full p-t-38">
                        <a href="#" class="flex-c-m how-pagination1 trans-04 m-all-7 active-pagination1">
                            1
                        </a>

                        <a href="#" class="flex-c-m how-pagination1 trans-04 m-all-7">
                            2
                        </a>
                    </div>
                </div>
        </section>
        <!-- Back to top -->
        <div class="btn-back-to-top" id="myBtn">
            <span class="symbol-btn-back-to-top">
                <i class="zmdi zmdi-chevron-up"></i>
            </span>
        </div>

        <!-- Modal1 -->
        <div class="wrap-modal1 js-modal1 p-t-60 p-b-20">
            <div class="overlay-modal1 js-hide-modal1"></div>

            <div class="container">
                <div class="bg0 p-t-60 p-b-30 p-lr-15-lg how-pos3-parent">
                    <button class="how-pos3 hov3 trans-04 js-hide-modal1">
                        <img src="images/icons/icon-close.png" alt="CLOSE">
                    </button>

                    <div class="row">
                        <div class="col-md-6 col-lg-7 p-b-30">
                            <div class="p-l-25 p-r-30 p-lr-0-lg">
                                <div class="wrap-slick3 flex-sb flex-w">
                                    <div class="wrap-slick3-dots"></div>
                                    <div class="wrap-slick3-arrows flex-sb-m flex-w"></div>

                                    <div class="slick3 gallery-lb">
                                        <div class="item-slick3" data-thumb="images/img1.jpg">
                                            <div class="wrap-pic-w pos-relative">
                                                <img src="images/img1.jpg" alt="IMG-PRODUCT">

                                                <a class="flex-c-m size-108 how-pos1 bor0 fs-16 cl10 bg0 hov-btn3 trans-04" href="images/product-detail-01.jpg">
                                                    <i class="fa fa-expand"></i>
                                                </a>
                                            </div>
                                        </div>

                                        <div class="item-slick3" data-thumb="images/img2.jpg">
                                            <div class="wrap-pic-w pos-relative">
                                                <img src="images/img2.jpg" alt="IMG-PRODUCT">

                                                <a class="flex-c-m size-108 how-pos1 bor0 fs-16 cl10 bg0 hov-btn3 trans-04" href="images/product-detail-02.jpg">
                                                    <i class="fa fa-expand"></i>
                                                </a>
                                            </div>
                                        </div>

                                        <div class="item-slick3" data-thumb="images/img3.jpg">
                                            <div class="wrap-pic-w pos-relative">
                                                <img src="images/img3.jpg" alt="IMG-PRODUCT">

                                                <a class="flex-c-m size-108 how-pos1 bor0 fs-16 cl10 bg0 hov-btn3 trans-04" href="images/product-detail-03.jpg">
                                                    <i class="fa fa-expand"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 col-lg-5 p-b-30">
                            <div class="p-r-50 p-t-5 p-lr-0-lg">
                                <h4 class="mtext-105 cl2 js-name-detail p-b-14">
                                    Lightweight Jacket
                                </h4>

                                <span class="mtext-106 cl2">
                                    $58.79
                                </span>

                                <p class="stext-102 cl3 p-t-23">
                                    Nulla eget sem vitae eros pharetra viverra. Nam vitae luctus ligula. Mauris consequat ornare feugiat.
                                </p>

                                <!--  -->
                                <div class="p-t-33">
                                    <div class="flex-w flex-r-m p-b-10">
                                        <div class="size-203 flex-c-m respon6">
                                            Size
                                        </div>

                                        <div class="size-204 respon6-next">
                                            <div class="rs1-select2 bor8 bg0">
                                                <select class="js-select2" name="time">
                                                    <option>Choose an option</option>
                                                    <option>Size S</option>
                                                    <option>Size M</option>
                                                    <option>Size L</option>
                                                    <option>Size XL</option>
                                                </select>
                                                <div class="dropDownSelect2"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="flex-w flex-r-m p-b-10">
                                        <div class="size-203 flex-c-m respon6">
                                            Color
                                        </div>

                                        <div class="size-204 respon6-next">
                                            <div class="rs1-select2 bor8 bg0">
                                                <select class="js-select2" name="time">
                                                    <option>Choose an option</option>
                                                    <option>Red</option>
                                                    <option>Blue</option>
                                                    <option>White</option>
                                                    <option>Grey</option>
                                                </select>
                                                <div class="dropDownSelect2"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="flex-w flex-r-m p-b-10">
                                        <div class="size-204 flex-w flex-m respon6-next">
                                            <div class="wrap-num-product flex-w m-r-20 m-tb-10">
                                                <div class="btn-num-product-down cl8 hov-btn3 trans-04 flex-c-m">
                                                    <i class="fs-16 zmdi zmdi-minus"></i>
                                                </div>

                                                <input class="mtext-104 cl3 txt-center num-product" type="number" name="num-product" value="1">

                                                <div class="btn-num-product-up cl8 hov-btn3 trans-04 flex-c-m">
                                                    <i class="fs-16 zmdi zmdi-plus"></i>
                                                </div>
                                            </div>

                                            <button class="flex-c-m stext-101 cl0 size-101 bg1 bor1 hov-btn1 p-lr-15 trans-04 js-addcart-detail">
                                                Add to cart
                                            </button>
                                        </div>
                                    </div>	
                                </div>

                                <!--  -->

                                <div class="flex-w flex-m p-l-100 p-t-40 respon7">
                                    <div class="flex-m bor9 p-r-10 m-r-11">
                                        <a href="#" class="fs-14 cl3 hov-cl1 trans-04 lh-10 p-lr-5 p-tb-2 js-addwish-detail tooltip100" data-tooltip="Add to Wishlist">
                                            <i class="zmdi zmdi-favorite"></i>
                                        </a>
                                    </div>

                                    <a href="#" class="fs-14 cl3 hov-cl1 trans-04 lh-10 p-lr-5 p-tb-2 m-r-8 tooltip100" data-tooltip="Facebook">
                                        <i class="fa fa-facebook"></i>
                                    </a>

                                    <a href="#" class="fs-14 cl3 hov-cl1 trans-04 lh-10 p-lr-5 p-tb-2 m-r-8 tooltip100" data-tooltip="Twitter">
                                        <i class="fa fa-twitter"></i>
                                    </a>

                                    <a href="#" class="fs-14 cl3 hov-cl1 trans-04 lh-10 p-lr-5 p-tb-2 m-r-8 tooltip100" data-tooltip="Google Plus">
                                        <i class="fa fa-google-plus"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

















        <footer class="bg3 p-t-75 p-b-32">
            <?php include('Footer.php') ?>
        </footer>

        <?php include('js.php') ?>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

        <!-- Popper JS -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>

        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>

        <script src="slick.min.js">
        </script>
        <script>
            $(document).ready(function () {



                $("#s2").slick({
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    // asNavFor: "#s1",

                    // arrows: true,
                    centerMode: true,
                    autoplay: true,
                    autoplaySpeed: 2000
                            // focusOnSelect: true
                            /* slidesToShow: 1,
                             slidesToScroll: 1,
                             asNavFor: "#s1",
                             arrows: false,
                             
                             dots: true,
                             centerMode: true,
                             focusOnSelect: true*/
                });
            });

        </script>




    </body>
</html>
<?php
if (isset($_POST['filter'])) {
    $m = $_POST['fmid'];
    //echo $_POST['fmid'];
    $c = $_POST['fcid'];
    //echo $_POST['fcid'];
    //echo "<br>OK";
    header("location:Home_customer.php?fcid=$c&fmid=$m");
} else {
    //echo "<br>Error";
}
?>