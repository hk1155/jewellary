<?php
session_start();

//include './Connection.php'; 
?>
<html lang="en">
    <head>
        <title>Home</title>
        <?php include('Head.php') ?>

    </head>

    <style>
        .header-v3 .wrap-menu-desktop{
            background-color: black;
        }
        h6.gt{
            text-align: center;


            position: relative;
        }

        h6
        {
            color: #000000;
            font-size: 20px;

            font-weight: 400;
            display: block;
            padding-bottom: 10px;
        }

        .before{content:'';
                background:url(home_line.png) no-repeat center;
                position:absolute;left:0;
                right:0;bottom:0;
                margin-bottom:-10px;
                width:100%;
                height:13px;
        }
    </style>
    <body class="animsition">

        <header class="header-v3">
            <?php include('Header.php') ?>
        </header>
        <?php include('Cart.php') ?>
        <br><br><br>


        <div class="bg0 m-t-23 p-b-140">
            <div class="container">

                <h6 class="gt">
                    New Collection
                    <div class="before">
                    </div>
                </h6><br>
                <div class="row isotope-grid">


                    <?php
                    // if (isset($_GET['vid'])) {
                    // $vid = $_GET['vid'];
                    include './Connection.php';
                    // $conn = new mysqli('Localhost', 'root', '', 'jewellary1');
                    // $sql = "select * from tbl_product where status=1";
                    $sql = "select tbl_product.*, tbl_pimage.* ,tbl_pcategory_type.cat_id,tbl_pcategory.name,tbl_material_type.type from tbl_product INNER JOIN tbl_pcategory_type on tbl_product.pct_id=tbl_pcategory_type.cat_type_id INNER join tbl_pcategory on tbl_pcategory_type.cat_id=tbl_pcategory.category_id INNER join tbl_material_type on tbl_pcategory_type.matel_id=tbl_material_type.mtype_id INNER JOIN tbl_pimage on  tbl_product.model_no=tbl_pimage.model_no where tbl_product.status=1 ORDER by tbl_product.model_no DESC LIMIT 8";
                    $res = mysqli_query($conn, $sql);

                    if (mysqli_num_rows($res)) {
                        //echo "Data Found";

                        while ($row = mysqli_fetch_assoc($res)) {
                            // echo $row['category_id'];
                            ?>  



                            <div class="col-sm-6 col-md-4 col-lg-3 p-b-35 isotope-item women">
                                <!-- Block2 -->
                                <div class="block2">
                                    <div class="block2-pic hov-img0 block1 wrap-pic-w label-new" style="" data-label="New" >
                                        <img src="<?php echo "admin/" . $row['image'] ?>" alt="IMG-PRODUCT">

                                        <a href='#' class="block2-btn flex-c-m stext-103 cl2 size-102 bg3 bor2 hov-btn3 p-lr-15 trans-04" style="color: white" data-toggle="modal"  data-target="#<?php echo "v_" . $row['model_no'] ?>">


                                            Quick View
                                        </a>


                                        <div class="modal fade" id="<?php echo "v_" . $row['model_no'] ?>" role="dialog">
                                            <div class="modal-dialog modal-dialog-centered modal-lg">
                                                <div class="modal-content">
                                                    <!-- <div class="modal-header">

                                                         <h4 class="modal-title">Modal Header</h4>
                                                         <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                     </div> -->
                                                    <div class="modal-body" style="width: 100%;height: 73%;">

                                                        <button type="button" class="close" data-dismiss="modal" style="float: right;">&times;</button>
                                                        <p>Model No : 
                                                            <?php echo "<b>" . $row['model_no'] . "</b>"; ?>
                                                        </p>
                                                        <div class="row">
                                                            <div class="col-md-6 col-lg-7 p-b-30">
                                                                <div class="p-l-25 p-r-30 p-lr-0-lg">
                                                                    <div class="wrap-slick3 flex-sb flex-w">
                                                                        <div class="wrap-slick3-dots"></div>
                                                                        <div class="wrap-slick3-arrows flex-sb-m flex-w"></div>

                                                                        <div class="slick3 gallery-lb " >
                                                                            <div class="item-slick3" data-thumb="<?php echo $row['image1'] ?>">
                                                                                <div class="wrap-pic-w pos-relative ">
                                                                                    <img src="<?php echo $row['image1'] ?>" alt="IMG-PRODUCT">

                                                                                    <a class="flex-c-m size-108 how-pos1 bor0 fs-16 cl10 bg0 hov-btn3 trans-04" href="<?php echo $row['image1'] ?>">
                                                                                        <i class="fa fa-expand"></i>
                                                                                    </a>
                                                                                </div>
                                                                            </div>

                                                                            <div class="item-slick3" data-thumb="<?php echo $row['image2'] ?>">
                                                                                <div class="wrap-pic-w pos-relative">
                                                                                    <img src="<?php echo $row['image2'] ?>" alt="IMG-PRODUCT">

                                                                                    <a class="flex-c-m size-108 how-pos1 bor0 fs-16 cl10 bg0 hov-btn3 trans-04" href="<?php echo $row['image2'] ?>">
                                                                                        <i class="fa fa-expand"></i>
                                                                                    </a>
                                                                                </div>
                                                                            </div>

                                                                            <div class="item-slick3" data-thumb="<?php echo $row['image3'] ?>">
                                                                                <div class="wrap-pic-w pos-relative">
                                                                                    <img src="<?php echo $row['image3'] ?>" alt="IMG-PRODUCT">

                                                                                    <a class="flex-c-m size-108 how-pos1 bor0 fs-16 cl10 bg0 hov-btn3 trans-04" href="<?php echo $row['image3'] ?>">
                                                                                        <i class="fa fa-expand"></i>
                                                                                    </a>
                                                                                </div>
                                                                            </div>

                                                                            <div class="item-slick3" data-thumb="<?php echo $row['image4'] ?>">
                                                                                <div class="wrap-pic-w pos-relative">
                                                                                    <img src="<?php echo $row['image4'] ?>" alt="IMG-PRODUCT">

                                                                                    <a class="flex-c-m size-108 how-pos1 bor0 fs-16 cl10 bg0 hov-btn3 trans-04" href="<?php echo $row['image4'] ?>">
                                                                                        <i class="fa fa-expand"></i>
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-6 col-lg-5 p-b-30">
                                                                <div class="p-r-50 p-t-5 p-lr-0-lg">
                                                                    <b><h4 class="mtext-104 cl2 js-name-detail p-b-14">
                                                                            <?php echo ucwords($row['type']) . " " . ucwords($row['name']) ?>
                                                                        </h4>
                                                                        <h4 class="mtext-104 cl2 js-name-detail p-b-14">
                                                                            <?php echo "Weight : " . $row['weight'] . " gm"; ?>
                                                                        </h4>
                                                                    </b>

                                                                    <span class="mtext-106 cl2">
                                                                        Price : <i class="fa fa-rupee"></i>&nbsp;<?php echo $row['price'] ?>
                                                                    </span>

                                                                    <p class="stext-102 cl3 p-t-23">
                                                                        Nulla eget sem vitae eros pharetra viverra. Nam vitae luctus ligula. Mauris consequat ornare feugiat.
                                                                    </p>

                                                                    <!--  -->
                                                                    <div class="p-t-33">


                                                                        <div class="flex-w flex-r-m p-b-10">
                                                                            <div class="size-204 flex-w flex-m respon6-next">
                                                                                <div class="wrap-num-product flex-w m-r-20 m-tb-10">
                                                                                    <div class="btn-num-product-down cl8 hov-btn3 trans-04 flex-c-m" onclick="minusquant()">
                                                                                        <i class="fs-16 zmdi zmdi-minus" ></i>
                                                                                    </div>

                                                                                    <input class="mtext-104 cl3 txt-center num-product" type="number" name="num-product" value="1" id="quant_<?php echo $row['model_no'] ?>">

                                                                                    <div class="btn-num-product-up cl8 hov-btn3 trans-04 flex-c-m" onclick="plusquant()">
                                                                                        <i class="fs-16 zmdi zmdi-plus" ></i>
                                                                                    </div>
                                                                                </div>
                                                                                <input type="hidden" name="hd" id="hd1_<?php echo $row['model_no'] ?>" value="<?php echo $row['quantity'] ?>">

                                                                                <?php if (isset($_SESSION['suname'])) { ?>
                                                                                    <p id="sc1"></p>
                                                                                    <input type="button" name="cart" value="Add to Cart" onclick="myfunction('<?php echo $row['model_no'] ?>')" class="flex-c-m stext-101 cl0 size-101 bg1 bor1 hov-btn1 p-lr-15 trans-04 js-addcart-detail" data-dismiss='modal'>
                                                                                <?php } else { ?>

                                                                                    <b><p style="color: red"> Please do Login then You Add The Product In Your Cart</p></b>
                                                                                <?php } ?>
                                                                            </div>
                                                                        </div>	
                                                                    </div>

                                                                    <!--  -->

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>


                                    </div>

                                    <div class="block2-txt flex-w flex-t p-t-14">
                                        <div class="block2-txt-child1 flex-col-l ">
                                            <a href="product-detail.html" class="stext-104 cl4 hov-cl1 trans-04 js-name-b2 p-b-6">
                                                <?php echo "<b>".ucwords($row['type'])."</b> ".ucwords($row['name']); ?>
                                            </a>

                                            <span class="stext-105 cl3">
                                                <i class="fa fa-rupee"></i><?php echo $row['price'] ?>
                                            </span>
                                        </div>

                                        <div class="block2-txt-child2 flex-r p-t-3">
                                            <a href="#" class="btn-addwish-b2 dis-block pos-relative js-addwish-b2">
                                                <img class="icon-heart1 dis-block trans-04" src="images/icons/icon-heart-01.png" alt="ICON">
                                                <img class="icon-heart2 dis-block trans-04 ab-t-l" src="images/icons/icon-heart-02.png" alt="ICON">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                    } else {
                        echo "<b style='color:red'>Product Not Exist!!</b>";
                    }
                    /* } else {
                      echo "No Record Found!!";
                      } */
                    ?>


                </div>


                <!-- Load more -->
                <!--  <div class="flex-c-m flex-w w-full p-t-45">
                      <a href="#" class="flex-c-m stext-101 cl5 size-103 bg2 bor1 hov-btn1 p-lr-15 trans-04">
                          Load More
                      </a>
                  </div> -->
            </div>
        </div>



        <footer class="bg3 p-t-75 p-b-32">
            <?php include('Footer.php') ?>
        </footer>
        <?php include('js.php') ?>
    </body>
</html>