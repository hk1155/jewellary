<!-- Sidebar -->
<?php include 'connection.php'; ?>
<aside class="wrap-sidebar js-sidebar">
    <div class="s-full js-hide-sidebar"></div>

    <div class="sidebar flex-col-l p-t-22 p-b-25">
        <div class="flex-r w-full p-b-30 p-r-27">
            <div class="fs-35 lh-10 cl2 p-lr-5 pointer hov-cl1 trans-04 js-hide-sidebar">
                <i class="zmdi zmdi-close"></i>
            </div>
        </div>

        <div class="sidebar-content flex-w w-full p-lr-65 js-pscroll">
            <ul class="sidebar-link w-full">
                <li class="p-b-13">
                    <a href="Home_Customer.php" class="stext-102 cl2 hov-cl1 trans-04">
                        Home
                    </a>
                </li>


                <?php if (isset($_SESSION['suname']) and $_SESSION['spass']) { ?>

                    <li class="p-b-13">
                        <a href="#" class="stext-102 cl2 hov-cl1 trans-04  js-hide-sidebar" data-toggle="modal" data-target="#myModal">
                            My Account
                        </a>
                    </li>

                    <li class="p-b-13">
                        <a href="#" class="stext-102 cl2 hov-cl1 trans-04  js-hide-sidebar" data-toggle="modal" data-target="#Change">
                            Change password
                        </a>
                    </li>

                    <li class="p-b-13">
                        <a href="#" class="stext-102 cl2 hov-cl1 trans-04  js-hide-sidebar" data-toggle="modal" data-target="#upload">
                            Upload Your Requriment Product
                        </a>
                    </li>
                <?php } ?>

            </ul>


        </div>
</aside>



<!--  Sidebar End -->

<!--  Cart -->  
<div class="wrap-header-cart js-panel-cart">
    <div class="s-full js-hide-cart"></div>

    <div class="header-cart flex-col-l p-l-65 p-r-25">
        <div class="header-cart-title flex-w flex-sb-m p-b-8">
            <span class="mtext-103 cl2">
                Your Cart
            </span>

            <div class="fs-35 lh-10 cl2 p-lr-5 pointer hov-cl1 trans-04 js-hide-cart">
                <i class="zmdi zmdi-close"></i>
            </div>
        </div>


        <?php
        if (isset($_SESSION['suname']) and $_SESSION['spass']) {
            $sum = 0;
            ?>


            


                    <div id="add1">

                    </div>



              

                

            <?php
        } else {
            echo "<b><p style='color:red'>After Login You Can Show Your Cart Details!!</p></b>";
        }
        ?>
    </div>
</div>
<!-- Cart Emd -->

<?php if (isset($_SESSION['suname']) and $_SESSION['spass']) { ?>


    <!--  MODEL FOR MYACCOUNT-->




    <div class="modal fade" id="myModal">
        <div class="modal-dialog modal-dialog-centered modal-md ">
            <div class="modal-content">

                <!-- Modal Header -->

                <div class="modal-header">
                    <h4 class="modal-title ">My Profile</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>


                <!-- Modal body -->
                <div class=" js-hide-sidebar"></div>
                <div class="modal-body">

                    <?php
                    if (isset($_SESSION['suname']) and $_SESSION['spass']) {



                        $sql = "SELECT * FROM tbl_customer where password='$p' && email='$u'";
                        $result = mysqli_query($conn, $sql);

                        //$result = $conn->query($sql);

                        if (mysqli_num_rows($result)) {

                            while ($row = mysqli_fetch_assoc($result)) {

                                //  echo $row['city_id'];
                                ?>
                                <form action="#" method="post" id="form1">
                                    <div class="login-form">

                                        <div class="row form-group">
                                            <div class="col col-md-12">
                                                <div class="input-group">
                                                    <input type="text" id="fname1" name="txtfname" class="form-control" placeholder="First Name" required pattern="^[A-Za-z]{3,50}$" title="Please Enter Valid Name" value="<?php echo $row['fname'] ?>">
                                                    &nbsp;<input type="text" id="mname1" name="txtmname" class="form-control" placeholder="Middle Name" required pattern="^[A-Za-z]{3,50}$" title="Please Enter Valid Name" value="<?php echo $row['mname'] ?>">
                                                    &nbsp;<input type="text" id="lname1" name="txtlname" minlength="3" class="form-control" placeholder="Last Name" required  pattern="^[A-Za-z]{3,50}$" title="Please Enter Valid Name" value="<?php echo $row['lname'] ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <textarea type="text" id="address1" name="txtaddress" placeholder="Address" class="form-control" required pattern="^[A-Za-z]{3,150}$" title="Please Enter Valid Address"><?php echo $row['address'] ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">


                                                <select name="txtcity"  class="form-control" id="city1" >

                                                    <?php
                                                    $sql1 = "select * from tbl_city";
                                                    $res1 = mysqli_query($conn, $sql1);



                                                    if (mysqli_num_rows($res1)) {
                                                        while ($row1 = mysqli_fetch_assoc($res1)) {

                                                            if ($row['city_id'] == $row1['city_id']) {

                                                                $se = "selected";
                                                            } else {
                                                                $se = "";
                                                            }
                                                            ?>

                                                            <option value="<?php echo $row1['city_id'] ?>" <?php echo $se ?>><?php echo $row1['city_name'] ?></option> 

                                                            <?php
                                                        }
                                                    } else {
                                                        echo "Data not Found";
                                                    }
                                                    ?>

                                                </select>


                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">

                                                <?php
                                                //  echo "Bhargav :".$row['gender'];
                                                if ($row['gender'] == 'm') {
                                                    $m = "checked";
                                                    $f = "";
                                                } else {
                                                    $m = "";
                                                    $f = "checked";
                                                }
                                                ?>
                                                Gender :&nbsp;<input type="radio" name="gender" id="g11"  value="m" <?php echo $m ?>  >&nbsp;Male&nbsp;
                                                &nbsp;<input type="radio" name="gender" id="g11" value="f" <?php echo $f ?>>&nbsp;Female

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" id="contact1" name="txtcontact" placeholder="Contact number" class="form-control" maxlength="10" minlength="10" onkeypress='return event.charCode >= 48 && event.charCode <= 57' pattern="[7986][0-9]{9}" title="Please Enter Valid Mobile Number" required value="<?php echo $row['contactno'] ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="email1" id="email" disabled name="txtemail" placeholder="Email" class="form-control" required pattern="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" title="Please Enter Valid Email Address" value="<?php echo $row['email'] ?>">
                                            </div>
                                        </div>

                                        <center> <input type="button" class="flex-c-m stext-103 cl0 size-121 bg3 bor2 hov-btn3 p-lr-15 trans-04" data-dismiss='modal'  style="width: 50%;" name="update" value="Update Information" onclick="profile(<?php echo $row['customer_id'] ?>)" ></center>

                                    </div>                                    
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!--End MYACCOUNT MODEL-->


                <!--MODEL FOR CHANGE PASSWORD -->
                <div class = "modal fade" id = "Change">
                    <div class = "modal-dialog modal-dialog-centered modal-md ">
                        <div class = "modal-content">

                            <!--Modal Header -->

                            <div class = "modal-header">
                                <h4 class = "modal-title text-center">Change Password</h4>
                                <button type = "button" class = "close" data-dismiss = "modal">&times;
                                </button>
                            </div>


                            <!--Modal body -->
                            <div class = " js-hide-sidebar"></div>
                            <div class = "modal-body">

                                <?php
                                if (isset($_SESSION['suname']) and $_SESSION['spass']) {
                                    ?>
                                    <form action="#" method="post" id="form1">
                                        <div class="login-form">

                                            <div class="row form-group">
                                                <div class="col col-md-12">
                                                    <div class="input-group">
                                                        <div class="input-group-addon"><i class="fa fa-star"></i></div>
                                                        <input type="text" id="current" name="txtcurrent" class="form-control" onchange="change(<?php echo $row['customer_id'] ?>)" placeholder="Current Password" required="" pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$" title="Please Enter Valid Current Password"> 
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-addon"><i class="fa fa-lock"></i></div>
                                                    <input type="password" id="password"  name="pw1" onkeyup='check();' placeholder="Password" class="form-control" pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-addon"><i class="fa fa-lock"></i></div>
                                                    <input type="password" id="confirm_password" onkeyup='check();' name="pw2" placeholder="Conform Password" class="form-control" required> 
                                                </div>
                                                <center><span id='message'></span></center>
                                            </div>
                                            <center><div id="dv" style="color: red"></div></center>
                                            <center> <input type="button" class="flex-c-m stext-103 cl0 size-121 bg3 bor2 hov-btn3 p-lr-15 trans-04" style="width: 50%;" data-dismiss='modal' onclick="change1(<?php echo $row['customer_id'] ?>)" name="chane" value="Change Password"></center>

                                        </div>
                                    </form>

                                    <?php
                                }
                                ?>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- END CHANGE PASSWORD MODEL-->


                <!-- MODEL FOR UPLOAD REQURIMENT -->
                <div class = "modal fade" id = "upload">
                    <div class = "modal-dialog modal-dialog-centered modal-md ">
                        <div class = "modal-content">

                            <!--Modal Header -->

                            <div class = "modal-header">
                                <h4 class = "modal-title text-center">Upload Your Requriment Product</h4>
                                <button type = "button" class = "close" data-dismiss = "modal">&times;
                                </button>
                            </div>


                            <!--Modal body -->
                            <div class = " js-hide-sidebar"></div>
                            <div class = "modal-body">

                                <?php
                                if (isset($_SESSION['suname']) and $_SESSION['spass']) {
                                    ?>
                                    <form action="#" method="post" id="form1">
                                        <div class="login-form">

                                            <div class="row form-group">
                                                <div class="col col-md-12">
                                                    <div class="input-group">
                                                        <select  class="form-control" name="txtmaterial" id="material">
                                                            <option value="">Select Material</option>
                                                        </select>
                                                        &nbsp;<select  class="form-control" name="txtcategory" id="category">
                                                            <option value="">Select Category</option>
                                                        </select>
                                                        &nbsp;<select  class="form-control" name="txtpurity" id="purity">
                                                            <option value="">--Select Purity--</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col col-md-12">
                                                    <div class="input-group">
                                                        <input type="text" id="weight" name="txtweight" placeholder="Enter Weight in GRAM" class="form-control" required> 
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col col-md-12">
                                                    <div class="input-group">
                                                        <input type="text" id="expect" name="txtexpect" placeholder="Enter Expected Price" class="form-control" required> 
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="row form-group">
                                                <div class="col col-md-12">
                                                    <div class="input-group">
                                                        <label>Upload Your Expected Product Image :</label>
                                                        <input type="file" id="image" name="txtimage"  required> 
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <center> <input type="button" class="flex-c-m stext-103 cl0 size-121 bg3 bor1 hov-btn3 p-lr-15 trans-04 pointer" style="width: 50%;" onclick="upload(<?php echo $row['customer_id'] ?>)" name="chane" value="Upload Information"  ></center>

                                        </div>
                                    </form>

                                    <?php
                                }
                                ?>
                            </div>

                        </div>
                    </div>
                </div>

                <!-- END UPLLOAD REQURIMENT MODEL-->



                <?php
            }
        } else {
            echo "0 results";
        }




        $conn->close();
    }
    ?>


    <!--CHECK PASSWORD AND CONFORMPASSWORD ARE MATCH OR NOT -->
    <script>
        var check = function () {


            if (document.getElementById('password').value == "" || document.getElementById('confirm_password').value == "")
            {
                document.getElementById('message').style.color = 'red';
                document.getElementById('message').innerHTML = 'Please Enter Password and Conform Password Both!!';
            } else
            {

                if (document.getElementById('password').value ==
                        document.getElementById('confirm_password').value) {
                    document.getElementById('message').style.color = 'green';
                    document.getElementById('message').innerHTML = 'Conform Password is matching to password';
                    // document.getElementById('message').innerHTML = 'Match';
                    return true;
                } else {
                    document.getElementById('message').style.color = 'red';
                    document.getElementById('message').innerHTML = 'Conform Password is not matching to password';
                    return false;
                }
            }
        }


        //window.load=ready();

    </script>
    <!-- END CHECK PASSWORD AND CONFORMPASSWORD ARE MATCH OR NOT -->

<?php } ?>
