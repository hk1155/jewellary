<?php
ob_start();
session_start();

if (isset($_SESSION['suname'])) {
   
} else {
     header('Location:Home_Customer.php');
}
//include './Connection.php';
?>
<html lang="en">
    <head>
        <title>Home</title>
        <?php include('Head.php') ?>

    </head>
    <style>
        .header-v3 .wrap-menu-desktop{
            background-color: black;
        }
    </style>
    <body class="animsition">

        <header class="header-v3">
            <?php include('Header.php') ?>
        </header>

        <div class="container-fluid"><br><br>
            <?php
            include('Cart.php');
            include './Connection.php';
            // $conn = new mysqli('Localhost', 'root', '', 'jewellary1');

            $s1 = $_SESSION['suname'];
            $p1 = $_SESSION['spass'];

            // echo $s1 . $p1;
            $sql3 = "select * from tbl_customer where email='$s1' && password='$p1'";
            $res3 = mysqli_query($conn, $sql3);

            if (mysqli_num_rows($res3)) {
                //echo "Record Found";
                $row3 = mysqli_fetch_assoc($res3);

                $cid = $row3['customer_id'];

                // echo $cid;
            }
            ?>

            <form class="bg0 p-t-75 p-b-85">
                <div class="">
                    <div class="row">
                        <div class="col-lg-10 col-xl-8 m-lr-auto m-b-50">
                            <div class="m-l-25 m-r--38 m-lr-0-xl">
                                <div class="wrap-table-shopping-cart">
                                    <table class="table-shopping-cart">
                                        <tr class="table_head">
                                            <th class="column-1">Product</th>
                                            <th class="column-2"></th>
                                            <th class="column-3">Price</th>
                                            <th class="column-4">Quantity</th>
                                            <th class="column-5">Total</th>

                                        </tr>


                                        <?php
                                        $sql2 = "select tbl_product.*,tbl_pcategory.name,tbl_material_type.type,tbl_cart.product_id,tbl_cart.Quantity,tbl_cart.total_amount from tbl_product INNER JOIN tbl_pcategory_type on tbl_product.pct_id=tbl_pcategory_type.cat_type_id INNER join tbl_pcategory on tbl_pcategory_type.cat_id=tbl_pcategory.category_id INNER join tbl_material_type on tbl_pcategory_type.matel_id=tbl_material_type.mtype_id INNER join tbl_cart  on  tbl_cart.product_id=tbl_product.model_no where tbl_cart.customer_id='$cid'";
                                        $res2 = mysqli_query($conn, $sql2);

                                        if (mysqli_num_rows($res2)) {
                                            $sum = 0;
                                            while ($row2 = mysqli_fetch_assoc($res2)) {
                                                ?>

                                                <tr class="table_row" id="<?php echo "remove_" . $row2['product_id'] ?>">
                                                    <td class="column-1" >
                                                        <div class="how-itemcart1" onclick="rcart('<?php echo $row2['product_id'] ?>')">
                                                            <img src="<?php echo "admin/" . $row2['image'] ?>" alt="IMG" >
                                                        </div>
                                                    </td>
                                                    <td class="column-2"><?php echo "<b class='text-success'>" . ucwords($row2['type']) . "</b> " . ucwords($row2['name']) ?><br><?php echo "Model No : " . $row2['product_id']; ?></td>
                                                    <td class="column-3"><i class="fa fa-rupee"></i>&nbsp;<?php echo $row2['price'] ?></td>
                                                    <td class="column-4">
                                                        <div class="wrap-num-product flex-w m-l-auto m-r-0">
                                                            <div class="btn-num-product-down cl8 hov-btn3 trans-04 flex-c-m"  onclick="down('<?php echo $row2['product_id'] ?>')">
                                                                <i class="fs-16 zmdi zmdi-minus"></i>
                                                            </div>

                                                            <input class="mtext-104 cl3 txt-center num-product" type="number" name="num-product1" value="<?php echo $row2['Quantity'] ?>" id="quant_<?php echo $row2['product_id'] ?>">

                                                            <div class="btn-num-product-up cl8 hov-btn3 trans-04 flex-c-m js-addcart-detail1" onclick="up('<?php echo $row2['product_id'] ?>')">
                                                                <i class="fs-16 zmdi zmdi-plus"></i>
                                                            </div>
                                                        </div>
                                                        <p id="error"></p>
                                                        <input type="hidden" name="tablequantity" id="tableq_<?php echo $row2['model_no'] ?>" value="<?php echo $row2['quantity'] ?>">
                                                    </td>
                                                    <td class="column-5">  <?php
                                                        $tam = $row2['price'] * $row2['Quantity'];
                                                            
                                                        $up=$row2['product_id'];
                                                        echo "<p id='updatep_$up'><i class='fa fa-rupee'></i>".$tam."</p>";

                                                       
                                                        
                                                        $sum = $sum + $tam;
                                                        ?></td>



                                                </tr>


                                                <?php
                                            }
                                        } else {
                                            echo "Record Not Found";
                                        }
                                        ?>


                                    </table>
                                </div>

                                <div class="flex-w flex-sb-m bor15 p-t-18 p-b-15 p-lr-40 p-lr-15-sm">
                                    <!-- <div class="flex-w flex-m m-r-20 m-tb-5">
                                         <input class="stext-104 cl2 plh4 size-117 bor13 p-lr-20 m-r-10 m-tb-5" type="text" name="coupon" placeholder="Coupon Code">
     
                                         <div class="flex-c-m stext-101 cl2 size-118 bg8 bor13 hov-btn3 p-lr-15 trans-04 pointer m-tb-5">
                                             Apply coupon
                                         </div>
                                     </div> -->

                                   <!-- <div class="flex-c-m stext-101 c12 size-119 bg8 bor13 hov-btn3 p-lr-15 trans-04 pointer m-tb-10">
                                        Update Cart
                                    </div> -->

                                    <div class="size-208">
                                        <span class="mtext-101 cl2">
                                            Total Amount&nbsp;&nbsp;:&nbsp; &nbsp;<p id="dis" ><i  class="fa fa-rupee"></i><?php echo " " . $sum ?></p>
                                        </span>
                                    </div>
                                   <a href="conform_order_and_address.php"class="flex-c-m stext-101 cl0 size-119  bg3 bor14 hov-btn3 p-lr-15 trans-04 pointer">
                                        Checkout
                                       </a>


                                </div>
                            </div>
                        </div>

                        <div class="col-sm-10 col-lg-7 col-xl-4 m-lr-auto m-b-50">
                            <div class="bor10 p-lr-40 p-t-30 p-b-40 m-l-63 m-r-40 m-lr-0-xl p-lr-15-sm">
                                <h4 class="mtext-109 cl2 p-b-30">
                                    Cart Totals
                                </h4>

                                <div class="flex-w flex-t bor12 p-b-13">
                                    <div class="size-208">
                                        <span class="stext-110 cl2">
                                            Subtotal:
                                        </span>
                                    </div>

                                    <div class="size-209">
                                        <span class="mtext-110 cl2">
                                            &nbsp; <p id="dis1"><i class="fa fa-rupee"></i>&nbsp;<?php echo $sum ?></p>
                                        </span>
                                    </div>
                                </div>

                                <div class="flex-w flex-t bor12 p-t-15 p-b-30">
                                    <div class="size-208 w-full-ssm">
                                        <span class="stext-110 cl2">
                                            Shipping:
                                        </span>
                                    </div>

                                    <div class="size-209 p-r-18 p-r-0-sm w-full-ssm">
                                        <p class="stext-111 cl6 p-t-2">
                                            <br><?php
                                            
                                            include './Connection.php';
                                            //echo $_SESSION['suname'];
                                            $e1=$_SESSION['suname'];
                                            
                                            $sql5="select * from tbl_customer where email='$e1'";
                                            $res5= mysqli_query($conn,$sql5);
                                            
                                            $row5= mysqli_fetch_assoc($res5);
                                            
                                            echo $row5['address'];
                                            
                                            $cno=$row5['contactno'];
                                            
                                            
                                            
                                            ?>
                                        </p>

                                        
                                    </div>
                                    <div class="size-208 w-full-ssm">
                                        <span class="stext-110 cl2">
                                             Contactno:
                                        </span>
                                    </div>
                                    <div class="size-209 p-r-18 p-r-0-sm w-full-ssm">
                                                 <p class="stext-111 cl6 p-t-2 " >
                                            
                                                        <br>+91 <?php echo  $cno?>
                                                        
                                            </p></div>
                                </div>

                                <div class="flex-w flex-t p-t-27 p-b-33">
                                    <div class="size-208">
                                        <span class="mtext-101 cl2">
                                            Total:
                                        </span>
                                    </div>

                                    <div class="size-209 p-t-1">
                                        <span class="mtext-110 cl2">
                                            <p id="dis2"><i  class="fa fa-rupee"></i> <?php echo $sum ?></p>
                                        </span>
                                    </div>
                                </div>

                                <a href="conform_order_and_address.php" class="flex-c-m stext-101 cl0 size-116 bg3 bor14 hov-btn3 p-lr-15 trans-04 pointer">
                                    Checkout
                                </a>
                            </div>
                        </div> 
                    </div>
                </div>
            </form>

        </div>



        <footer class="bg3 p-t-75 p-b-32">
            <?php include('Footer.php') ?>
        </footer>
        <?php include('js.php') ?>

    </body>
</html>

