<?php
ob_start();
session_start();

if (isset($_SESSION['suname_admin'])) {
    
} else {
    header('Location: index.php');
}
?>

<?php
include './connection.php';


date_default_timezone_set('Asia/Kolkata');

//$curdatetime= date('d-m-Y h:i');
//echo $curdatetime;
//exit();

$curdate = date('Y-m-d');

$view = "SELECT rate from tbl_material_type where type='gold'";

$res = mysqli_query($con, $view);


$viewsilver = "SELECT rate from tbl_material_type where type='silver'";

$ressilver = mysqli_query($con, $viewsilver);

//--------------------------------------------------------------

$av = "";
$dataview = "";

//$viewtable = "SELECT tbl_customize_product.*,tbl_customer.fname,lname,tbl_pcategory_type.*,tbl_pcategory.name,tbl_material_type.type FROM tbl_customize_product INNER JOIN tbl_customer ON tbl_customize_product.customer_id=tbl_customer.customer_id INNER JOIN tbl_pcategory_type ON tbl_pcategory_type.cat_type_id=tbl_customize_product.pctid INNER JOIN tbl_pcategory ON tbl_pcategory.category_id=tbl_pcategory_type.cat_id INNER JOIN tbl_material_type ON tbl_material_type.mtype_id=tbl_pcategory_type.matel_id WHERE tbl_customize_product.add_on='" . $curdate . "' and tbl_customize_product.status='pending'";
$viewtable = "SELECT tbl_customize_product.*,tbl_customer.fname,lname,tbl_pcategory_type.*,tbl_pcategory.name,tbl_material_type.type FROM tbl_customize_product INNER JOIN tbl_customer ON tbl_customize_product.customer_id=tbl_customer.customer_id INNER JOIN tbl_pcategory_type ON tbl_pcategory_type.cat_type_id=tbl_customize_product.pctid INNER JOIN tbl_pcategory ON tbl_pcategory.category_id=tbl_pcategory_type.cat_id INNER JOIN tbl_material_type ON tbl_material_type.mtype_id=tbl_pcategory_type.matel_id WHERE tbl_customize_product.status='pending' and tbl_customize_product.add_on='".$curdate."'";
$restable = mysqli_query($con, $viewtable);

if (mysqli_num_rows($restable) > 0) {
    $av = 1;

    while ($rowdata = mysqli_fetch_assoc($restable)) {

        $fname = $rowdata['fname'];
        $lname = $rowdata['lname'];

        $fullname = $fname . " " . $lname;
        $dataview .= "<tr id='strow" . $rowdata['cupid'] . "'>";

        $dataview .= "<td>";
        $dataview .= "<a href='".$rowdata['image'] ."'><img src='" . $rowdata['image'] . "' class='rounded zoom_home'></a>";
        $dataview .= "</td>";

        $dataview .= "<td>";
        //$dataview .= ucfirst($fullname);
        $dataview .= "<a href='javascript:void(0);' title='Customer Information'  data-toggle='modal' data-target='#c_" . $rowdata['customer_id'] . "' style='color:blue;'>" . ucfirst($fullname) . "</a>";
        $dataview .= "</td>";

        $dataview .= "<td>";
        $dataview .= ucfirst($rowdata['purity_id']);
        $dataview .= "</td>";

        $dataview .= "<td>";
        $dataview .= ucfirst($rowdata['type']);
        $dataview .= "</td>";

        $dataview .= "<td>";
        $dataview .= ucfirst($rowdata['name']);
        $dataview .= "</td>";

        $dataview .= "<td>";
        $dataview .= ucfirst($rowdata['quantity']);
        $dataview .= "</td>";

        $dataview .= "<td>";
        $dataview .= ucfirst($rowdata['weight']);
        $dataview .= "</td>";



        $dataview .= "<td>";
        $dataview .= ucfirst($rowdata['price']);
        $dataview .= "</td>";


        $dataview .= "<td>";
        if ($rowdata['status'] == 'pending') {

            $dataview .= " <div c  style='color: orangered;'>" . ucfirst($rowdata['status']) . "</div>";
        } else {

            $dataview .= " <div id='sta" . $rowdata['cupid'] . "'>" . ucfirst($rowdata['status']) . "</div>";
        }
        $dataview .= "</td>";

        $dataview .= "<td>";
        $dataview .= $rowdata['add_on'];
        $dataview .= "</td>";

        $dataview .= "<td>";
        $dataview .= "<center>";
        $dataview .= "<a href='javascript:void(0);'id='cc' onclick='fncuz(" . $rowdata['cupid'] . ");'  style='margin-right:20px;'><i class='fa fa-check' title='Accept Order' style='color:green;font-size:20px;'></i></a>";
        $dataview .= "<a href='javascript:void(0);' onclick='fnreject(" . $rowdata['cupid'] . ");'  style='margin-right:20px;'><i class='fa fa-close' title='Reject Order' style='color:red;font-size:20px;'></i></a>";
        // $dataview .= "<a href='javascript:void(0);' onclick='fncuz(" . $rowdata['cupid'] . ");'  style='margin-right:20px;'><i class='fa fa-eye' style='color:black;font-size:20px;'></i></a>";
        $dataview .= "</center>";
        $dataview .= "</td>";



        $dataview .= "</tr>";
    }
} else {

    $av = 0;
    $dataview .= "<tr>";
    $dataview .= "<td>";
    $dataview .= "No Records Found!!";
    $dataview .= "</td>";
    $dataview .= "</tr>";
}
?>



<html>
    <head>
        <title>Home | Admin Jewellary</title>

        <?php
        include('head.php');
        ?>


        <style>
            .zoom_home:hover {
                -ms-transform: scale(3.5); /* IE 9 */
                -webkit-transform: scale(3.5); /* Safari 3-8 */
                transform: scale(3.5); 
            </style>
        </head>
        <body>

            <?php
            include('left.php');
            ?>

            <!-- Right Panel --> 
            <div id="right-panel" class="right-panel">
                <?php
                include('header.php');
                ?>

                <div class="content pb-0">

                    <section>

                        <?php
                        // echo $rowdata['customer_id'];

                        $sqlcustomer = "select *from tbl_customer";
                        $rescust = mysqli_query($con, $sqlcustomer);

                        if (mysqli_num_rows($rescust)) {
                            while ($row = mysqli_fetch_assoc($rescust)) {
                                ?>


                                <div class="modal fade" id="c_<?php echo $row['customer_id'] ?>">
                                    <div class="modal-dialog">
                                        <div class="modal-content">

                                            <!-- Modal Header -->
                                            <div class="modal-header">
                                                <h5 class="modal-title" style="color: #0087ba;"><u>Customer Details</u></h5>
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>

                                            </div>

                                            <!-- Modal body -->
                                            <div class="modal-body" style="background-color: lightyellow;">
                                                <div class="table-responsive">
                                                    <table class="table-bordered">

                                                        <tr>  
                                                            <td width="40%">Name</td>  
                                                            <?php
                                                            $fullname = $row['fname'] . " " . $row['mname'] . " " . $row['lname'];
                                                            ?>
                                                            <td width="80%"><b><?php echo ucfirst($fullname); ?></b></td>  
                                                        </tr> 
                                                        <tr>  
                                                            <td width="30%">Gender</td> 
                                                            <?php
                                                            if ($row['gender'] == 'm') {
                                                                $g = "male";
                                                            } else {
                                                                $g = "female";
                                                            }
                                                            ?>
                                                            <td width="70%"><b><?php echo ucfirst($g); ?></b></td>  
                                                        </tr> 
                                                        <tr>  
                                                            <td width="30%">ContactNo</td>  
                                                            <td width="70%"><b><?php echo ucfirst($row['contactno']); ?></b></td>  
                                                        </tr> 
                                                        <tr>  
                                                            <td width="30%">E-mail</td>  
                                                            <td width="70%"><b><?php echo $row['email']; ?></b></td>  
                                                        </tr> 
                                                        <tr>  
                                                            <td width="30%">Address</td>  
                                                            <td width="70%"><b><?php echo $row['address']; ?></b></td>  
                                                        </tr> 


                                                    </table>
                                                </div>
                                            </div>

                                            <!-- Modal footer -->
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <?php
                            }
                        }
                        ?>






                        <!-- Widgets  -->
                        <div class="row">
                            <div class="col-lg-3 col-md-6">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="stat-widget-five">
                                            <div class="stat-icon dib flat-color-1">
                                                <i class="pe-7f-cash"></i>
                                            </div>
                                            <div class="stat-content">
                                                <div class="text-left dib"> 
                                                    <div class="stat-text">₹<?php
                                                        if (mysqli_num_rows($res)) {
                                                            $data = mysqli_fetch_assoc($res);
                                                            echo $data['rate'];
                                                        } else {
                                                            echo $data = '----';
                                                        }
                                                        ?>

                                                    </div>
                                                    <div class="stat-heading">Current Gold Price</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-6">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="stat-widget-five">
                                            <div class="stat-icon dib flat-color-2">
                                                <i class="pe-7f-cart"></i>
                                            </div>
                                            <div class="stat-content">
                                                <div class="text-left dib">
                                                    <div class="stat-text">₹<?php
                                                        if (mysqli_num_rows($ressilver)) {
                                                            $data = mysqli_fetch_assoc($ressilver);
                                                            echo $data['rate'];
                                                        } else {
                                                            echo $data = '----';
                                                        }
                                                        ?>



                                                    </div>
                                                    <div class="stat-heading">Current Silver Prices</div> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>



                            <div class="col-lg-3 col-md-6">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="stat-widget-five">
                                            <div class="stat-icon dib flat-color-4">
                                                <i class="pe-7f-users"></i>
                                            </div>
                                            <div class="stat-content">
                                                <div class="text-left dib"> 
                                                    <div class="stat-text"><span class="count">2986</span></div>
                                                    <div class="stat-heading">Registered Client</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <!-- Widgets End -->




                        <div class="clearfix"></div>
                        <div class="orders">
                            <div class="row">
                                <div class="col-xl-12"> 
                                    <div class="card">
                                        <div class="card-body">
                                            <h4 class="box-title">Today's Orders </h4>
                                        </div>
                                        <div class="card-body--">
                                            <div class="table-stats order-table ov-h">
                                                <table class="table table-hover table-bordered">
                                                    <thead>
                                                        <tr>
                                                          <!--  <th class="serial">#</th> -->
                                                            <th class="avatar">Image</th>
                                                            <th>Name</th>
                                                            <th>Fineness</th>
                                                            <th>Type</th>
                                                            <th>Category</th>
                                                            <th>Quantity</th>
                                                            <th>Weight</th>
                                                            <th>Price</th>
                                                            <th>Status</th>

                                                            <th>Adding Date</th>
                                                            <th>Action</th>

                                                        </tr>
                                                    </thead>
                                                    <tbody> 
                                                        <?php
                                                        if ($av == 1) {
                                                            echo $dataview;
                                                        } else {
                                                            echo $dataview;
                                                        }
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div> <!-- /.table-stats -->
                                        </div>
                                    </div> <!-- /.card -->
                                </div>  <!-- /.col-lg-8 -->


                            </div> 
                        </div> <!-- /.order -->



                    </section>


                </div>



                <div class="clearfix"></div>

                <?php
                include ('footer.php');
                ?>

            </div>
            <?php
            include('script.php');
            ?>
            <div id="container">



            </div>



        </body>

    </html>
