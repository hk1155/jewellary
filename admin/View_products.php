<?php
ob_start();
session_start();
if (isset($_SESSION['suname_admin'])) {
    
} else {
    header('Location: index.php');
}

include_once './connection.php';

$data = "";
$c = "";
$curdate = date('Y-m-d');

$select = "SELECT * FROM tbl_product order by add_on desc";
$res = mysqli_query($con, $select);

if (mysqli_num_rows($res) > 0) {
    $c = 1;
    while ($rowproduct = mysqli_fetch_assoc($res)) {
        $ad = $rowproduct['add_on'];

        if ($ad == $curdate) {
            $data .= "<tr class='table-success'>";
        }

        $data .= "<td>";
        $data .= $rowproduct['model_no'];
        $data .= "</td>";

        $data .= "<td>";
        $data .= $rowproduct['weight'];
        $data .= "</td>";

        $data .= "<td>";
        $data .= $rowproduct['purity_id'];
        $data .= "</td>";

        $data .= "<td>";
        $data .= $rowproduct['price'];
        $data .= "</td>";

        $data .= "<td>";
        $data .= "<center>";
        $data .= "<img src='" . $rowproduct['image'] . "' style='height:60px; width:60px;'>";
        $data .= "</center>";
        $data .= "</td>";

        $data .= "<td>";
        $data .= $rowproduct['quantity'];
        $data .= "</td>";

        $data .= "<td>";
        $data.="<center>";
        if ($rowproduct['status'] == 1) {
            $data .= "<div  id='togle1" . $rowproduct['model_no'] . "'><a href='javascript:void(0);' style='color:green;' id='togle' onclick='fntogle(\"" . $rowproduct['model_no'] . "\"," . $rowproduct['status'] . ");'><i class='fa fa-toggle-on'></i></a></div>";
        } else {
            //$data .= "<div  id='togle1" . $rowproduct['model_no'] . "'><a href='javascript:void(0);' id='togle' style='color:orange;' onclick=fntogle('" . $rowproduct['model_no'] . "','" . $rowproduct['status'] . "') ><i class='fa fa-toggle-off'></i></a></div>";
            $data .= "<div  id='togle1" . $rowproduct['model_no'] . "'><a href='javascript:void(0);' style='color:orange;' id='togle' onclick='fntogle(\"" . $rowproduct['model_no'] . "\"," . $rowproduct['status'] . ");'><i class='fa fa-toggle-off'></i></a></div>";
        }
       $data.="</center>";
        $data .= "</td>";
        
        $data .= "<td>";
         $data .= "<div><a href='" . $rowproduct['image'] . "' download style='color:purple; margin:5px;'><i class='fa fa-download'></i></a></div>";
        $data .= "</td>";

        $data .= "</tr>";
    }
} /*else {

    $data .= "<tr>";
    $data .= "<td>";
    $data .= "No Records Found!!";
    $data .= "</td>";
    $data .= "</tr>";
}*/
?>


<html>
    <head>
        <title>The Jewellary</title>
        <?php
        include('head.php');
        ?>



    </head>
    <body>
        <?php
        include('left.php');
        ?>

        <!-- Right Panel --> 
        <div id="right-panel" class="right-panel">
            <?php
            include('header.php');
            ?>

            <div class="content pb-0"> 

                <section>
                    <div class="content">
                        <div class="animated fadeIn">
                            <div class="row">

                                <div class="col-md-12">
                                    <div class="card">

                                        <div class="card-header col-12 col-md-12">

                                            <div class="row" >
                                                <div class="col-lg-8"><strong class="card-title col-md-8">Product Details</strong></div>

                                                <div class="col-sm-4"><a href="add_product.php"><i class="fa fa-plus btn btn-primary pull-right" style="color: #FFE4E1;"> Add New Product </i></a></div>
                                            </div>



                                        </div>
                                        <div class="card-body">
                                            <table id="bootstrap-data-table" class="table table-light table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>Model no</th>
                                                        <th>Weight</th>
                                                        <th>Fineness</th>
                                                        <th>Price</th>
                                                        <th>Image</th>
                                                        <th>Quantity</th>
                                                        <th>Status</th>
                                                        <th>Action</th>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    if ($c == 1) {
                                                        echo $data;
                                                    } else {
                                                        echo $data;
                                                    }
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div><!-- .animated -->
                    </div><!-- .content -->



                </section>
            </div>
            <div class="clearfix"></div>

            <?php
            include ('footer.php');
            ?>

        </div>
        <?php
        include('script.php');
        ?>
        <div id="container">



        </div>

        <script type="text/javascript">
            $(document).ready(function () {
                $('#tblid').DataTable();
            });
        </script>

    </body>

</html>
