<?php
ob_start();
session_start();
if (isset($_SESSION['suname_admin']) == "") {
    header('Location: admin_login.php');
}

include_once './connection.php';

$var = "";
$cp = "";

if (isset($_POST['btnchange'])) {
    $check = "SELECT * FROM tbl_login WHERE role='admin'";


    /* $cur = base64_encode($_POST['txtcurpassword']);
      $new = base64_encode($_POST['txtnewpassword']);
      $cnp = base64_encode($_POST['txtconpassword']); */

    $cur = md5($_POST['txtcurpassword']);
    $new = md5($_POST['txtnewpassword']);
    $cnp = md5($_POST['txtconpassword']);




    $res = mysqli_query($con, $check);

    if (mysqli_num_rows($res) > 0) {
        $row = mysqli_fetch_assoc($res);

        if ($row['password'] == $cur) {
            if ($new == $cnp) {
                $update = "UPDATE tbl_login SET password='" . $cnp . "' WHERE role='admin'";
                if (mysqli_query($con, $update)) {
                    $var = 1;
                } else {
                    $var = 0;
                }
            } else {
                $cp = 0;
            }
        } else {

            echo "current password not match";
            exit();
        }
    }
}
?>
<html>
    <head>
        <title>The Jewellary</title>
        <?php
        include('head.php');
        ?>



    </head>
    <body>
        <?php
        include('left.php');
        ?>

        <!-- Right Panel --> 
        <div id="right-panel" class="right-panel">
            <?php
            include('header.php');
            ?>

            <div class="content pb-0">

                <section>
                    <center>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="card">
                                    <div class="card-header">Change Password</div>
                                    <div class="card-body card-block">

                                        <form action="#" method="post" class="">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-addon"><i class="fa fa-asterisk"></i></div>
                                                    <input type="password" id="txtcurpassword" name="txtcurpassword" placeholder="Enter Your Current Password" class="form-control" required="required">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="input-group">

                                                    <input type="password" id="txtnewpassword" name="txtnewpassword" placeholder="New Password" class="form-control" required="required">
                                                    <div class="input-group-addon"><i class="fa fa-asterisk"></i></div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="input-group">

                                                    <input type="password" id="txtconpassword" name="txtconpassword" placeholder="Confirm Password" class="form-control" required="required">
                                                    <div class="input-group-addon"><i class="fa fa-asterisk"></i></div>
                                                </div>
                                            </div>
                                            <div class="form-actions form-group"><button type="submit" name="btnchange" class="btn btn-success btn-sm">Change password</button></div>
                                            <?php
                                            if ($var == 1) {
                                                ?><h4 style="color: green;">Updated Successfully!</h4>

                                                <?php
                                                if ($cp == 0) {
                                                    ?><h5 style="color: red;">Password & conform password not match</h5>

                                                    <?php
                                                }
                                            }
                                            ?>
                                        </form>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </center>
                </section>


            </div>



            <div class="clearfix"></div>

            <footer class="site-footer">
                <div class="footer-inner bg-white">
                    <div class="row">
                        <div class="col-sm-6">
                            Copyright &copy; 2018 The Jewellary
                        </div>
                        <div class="col-sm-6 text-right">
                            Designed by Simex ltd.
                        </div>
                    </div>
                </div>
            </footer>

        </div>
        <?php
        include('script.php');
        ?>


        <div id="container">



        </div>

    </body>

</html>
