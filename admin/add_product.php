<?php
ob_start();
session_start();
if (isset($_SESSION['suname_admin'])) {
    
} else {
    header('Location: index.php');
}
include_once './connection.php';
$datamatel = "";
$imagefiletype = "";
$check = "";
$status = "1";

$curdate = date('Y-m-d');
$view = "select tbl_pcategory_type.* , tbl_material_type.type,rate, mtype_id FROM tbl_pcategory_type INNER JOIN tbl_material_type on tbl_pcategory_type.matel_id=tbl_material_type.mtype_id group by tbl_material_type.type";
$resultview = mysqli_query($con, $view);

if (isset($_POST['btninsert']) && $_FILES['mainfile']) {
    $modelno = $_POST['txtmodel'];

    $dir = "Product_images/";
    $filename = $_FILES['mainfile']['name'];
    $temp = $_FILES['mainfile']['tmp_name'];
    $size = $_FILES['mainfile']['size'];
    $type = $_FILES['mainfile']['type'];
    $imagefiletype = strtolower(pathinfo($filename, PATHINFO_EXTENSION));
    $targetfile = $dir . $modelno;

    if (!file_exists($targetfile)) {
        mkdir($targetfile);
    }

    if ($imagefiletype != 'jpg' && $imagefiletype != 'jpeg' && $imagefiletype != 'png') {
        echo "Only jpg or png files Allowed";
    } else {


        move_uploaded_file($temp, $targetfile . "/" . $filename);
        $dataimage = $targetfile . "/" . $filename;
        $insert = "INSERT INTO tbl_product(model_no,image,pct_id,weight,purity_id,price,quantity,status,add_on) VALUES('" . $_POST['txtmodel'] . "','" . $dataimage . "','" . $_POST['dwcategory'] . "','" . $_POST['txtweight'] . "','" . $_POST['txtpurity'] . "','" . $_POST['txtmrp'] . "','" . $_POST['txtquantity'] . "','" . $status . "','" . $curdate . "')";
        $reinsert = mysqli_query($con, $insert);

        if ($reinsert) {



            //-----------------------------------------------------------------------------

            $filename2 = $_FILES['im2']['name'];
            $temp = $_FILES['im2']['tmp_name'];
            $size = $_FILES['im2']['size'];
            $type = $_FILES['im2']['type'];

            $imagefiletype = strtolower(pathinfo($filename2, PATHINFO_EXTENSION));
            $targetfile = $dir . $modelno;
            move_uploaded_file($temp, $targetfile . "/" . $filename2);
            $dataimage2 = $targetfile . "/" . $filename2;

            //-----------------------------------------------------------------------------

            $filename3 = $_FILES['im3']['name'];
            $temp = $_FILES['im3']['tmp_name'];
            $size = $_FILES['im3']['size'];
            $type = $_FILES['im3']['type'];

            $imagefiletype = strtolower(pathinfo($filename3, PATHINFO_EXTENSION));
            $targetfile = $dir . $modelno;
            move_uploaded_file($temp, $targetfile . "/" . $filename3);
            $dataimage3 = $targetfile . "/" . $filename3;

            //-----------------------------------------------------------------------------

            $filename4 = $_FILES['im4']['name'];
            $temp = $_FILES['im4']['tmp_name'];
            $size = $_FILES['im4']['size'];
            $type = $_FILES['im4']['type'];

            $imagefiletype = strtolower(pathinfo($filename4, PATHINFO_EXTENSION));
            $targetfile = $dir . $modelno;
            move_uploaded_file($temp, $targetfile . "/" . $filename4);
            $dataimage4 = $targetfile . "/" . $filename4;

            //-----------------------------------------------------------------------------




            $insert1 = "INSERT INTO tbl_pimage(model_no,image1,image2,image3,image4) VALUES('" . $_POST['txtmodel'] . "','" . $dataimage . "','" . $dataimage2 . "','" . $dataimage3 . "','" . $dataimage4 . "')";
            print_r($insert1);
            //exit();
            if ($rimg1 = mysqli_query($con, $insert1)) {
                header('Location:View_products.php');
            } else {
                echo "Something Went Wrong!!";
            }
        } else {
            echo "Not Insertd";
            exit();
        }
    }
}
?>
<html>
    <head>
        <title>The Jewellary</title>
        <?php
        include('head.php');
        ?>
    </head>
    <body>
        <?php
        include('left.php');
        ?>
        <!-- Right Panel --> 
        <div id="right-panel" class="right-panel">
            <?php
            include('header.php');
            ?>
            <div class="content pb-0"> 
                <section>
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <center><strong>New</strong> Product</center>
                            </div>
                            <div class="card-body card-block">
                                <form action="#" method="post" class="form-horizontal" id="uploadForm" enctype="multipart/form-data">
                                    <div class="row form-group">
                                        <div class="col-12 col-md-4">
                                            <select name="dwmatel"  id="dwmatel" class="form-control dwmatel" required="">
                                                <option value=" ">--Select Matel--</option>
                                                <?php
                                                if ($resultview) {
                                                    while ($rowmatel = mysqli_fetch_array($resultview)) {
                                                        $matel = ucfirst($rowmatel['type']);
                                                        $matelid = $rowmatel['mtype_id'];
                                                        ?>
                                                        <option value="<?php echo $matelid ?>"><?php echo $matel; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-12 col-md-4">
                                            <select name="dwcategory" id="dwcategory"  class="form-control" required="">
                                                <option value=" ">--Select Category--</option>
                                            </select>
                                        </div>
                                        <!--  <div class="col-12 col-md-4">
                                              <select name="dwfineness" id="dwfineness" class="form-control" required="">
                                                  <option value=" ">--Select Fineness--</option>
                                              </select>
                                          </div> -->

                                        <div class="form-group col-12 col-md-4">
                                            <div><input type="text"  name="txtpurity" readonly="" id="txtpurity" placeholder="Fineness of metal" class="form-control" required=""></div>

                                        </div>

                                    </div>
                                    <div class="row form-group">
                                        <div class="col-12 col-md-6"><input type="text"  name="txtmodel" disabled="" id="txtmodel" placeholder="Model no." class="form-control" required=""><small class="help-block form-text">Model no. depend on matel & category</small></div>
                                        <div class="col-12 col-md-6"><input type="text" id="txtweight" name="txtweight" disabled=""  placeholder="Weight only matel's" onchange="fnmrp();"  class="form-control" required=""><small class="help-block form-text">Doesn't Included extra materials!!</small></div>
                                    </div>

                                    <div class="row form-group">
                                        <div class="col-12 col-md-4"><input type="text" name="txtprice" placeholder="Matel price per Gram" class="form-control" id="txtprice" readonly=""></div>
                                        <div class="col-12 col-md-4"><input type="text" id="txtquantity" name="txtquantity" placeholder="Product Quantity" class="form-control" required=""><small class="help-block form-text">Quantity per Product</small></div>
                                        <div class="col-12 col-md-4"><input type="text"  name="txtmrp" placeholder="Final Price" id="txtmrp" class="form-control" readonly="" required=""><small class="help-block form-text">Matel Price * Weight</small></div>
                                    </div><hr>
                                    <div class="row form-group">
                                        <div class="col-12 col-md-2"><label for="file-input" class=" form-control-label">Main Image</label></div>
                                        <div class="col-12 col-md-4"><input type="file" id="file" name="mainfile" required="" accept=".jpg" class="form-control-file"></div>
                                        <div id="preview">
                                        </div>
                                    </div><hr><center>More Images</center><hr>

                                    <div class="row form-group">
                                        <div class="col-12 col-md-3"><label for="file-input" class=" form-control-label">1.</label></div>
                                        <div class="col-12 col-md-6"><input type="file" id="file-input" name="im2" class="form-control-file"></div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-12 col-md-3"><label for="file-input" class=" form-control-label">2.</label></div>
                                        <div class="col-12 col-md-6"><input type="file" id="file-input" name="im3" class="form-control-file"></div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-12 col-md-3"><label for="file-input" class=" form-control-label">3.</label></div>
                                        <div class="col-12 col-md-6"><input type="file" id="file-input" name="im4" class="form-control-file"></div>
                                    </div><hr>
                                    <center>
                                        <div class="col col-md-12">
                                            <input type="submit"  class="btn btn-success" name="btninsert" value="Add Product">
                                        </div>
                                    </center>
                                </form>
                            </div>                    
                        </div>
                    </div>
                </section>
            </div>

            <div class="clearfix"></div>

            <?php
            include ('footer.php');
            ?>

        </div>
        <?php
        include('script.php');
        ?>
        <div id="container">



        </div>

    </body>

</html>
