<?php
ob_start();
session_start();
$x = 0;
if (isset($_SESSION['suname_admin']) && isset($_SESSION['spass_admin'])) {

    header('Location: admin_home.php');
} else {
    
}
include_once './connection.php';

if (isset($_POST['btnlogin'])) {

    $username = $_POST['txtusername'];
    //echo $_POST['txtpassword'] . "<br>";
    $password = md5($_POST['txtpassword']);

   // echo $username;
    //echo $password;

    $sql = "SELECT * FROM tbl_login where username='$username' and password='$password' and role='admin'";
    $res = mysqli_query($con, $sql);
    if (mysqli_num_rows($res)) {
        $_SESSION['suname_admin'] = $_POST['txtusername'];
        $_SESSION['spass_admin'] = $_POST['txtpassword'];
        header("Location: admin_home.php");
    } else {
        $x = 1;
    }
}
?>

<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Admin Login</title>
        <meta name="description" content="Ela Admin - HTML5 Admin Template">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="images/alogo.png">
        <link rel="shortcut icon" href="images/halogo.png"> 

        <link rel="stylesheet" href="assets/css/normalize.css">
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/css/themify-icons.css">
        <link rel="stylesheet" href="assets/css/pe-icon-7-filled.css">
        <link rel="stylesheet" href="assets/css/flag-icon.min.css">
        <link rel="stylesheet" href="assets/css/cs-skin-elastic.css">
        <!-- <link rel="stylesheet" href="assets/css/bootstrap-select.less"> -->
        <link rel="stylesheet" href="assets/css/style.css">

        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

    <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script> -->

    </head>
    <body class="bg-light">


        <div class="sufee-login d-flex align-content-center flex-wrap ">
            <div class="container">
                <div class="login-content col-lg-2">

                    <div class="login-form">
                        <form action="#" method="post">

                            <div class="login-logo">
                                <a href="#">
                                    <img class="align-content" src="images/logo11.png" alt="images/logo11.png">
                                </a>
                            </div>


                            <div class="row form-group">
                                <div class="col col-md-12">
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-user"></i></div>
                                        <input type="email" id="input1-group1" name="txtusername" class="form-control" placeholder="Username" required="required">
                                    </div>
                                </div>
                            </div>



                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-asterisk"></i></div>
                                    <input type="password" id="txtpassword" name="txtpassword" placeholder="Password" class="form-control" required="required">

                                </div>
                                <center><h5 style="color: red;"><?php
                                        if ($x == 1) {
                                            echo "Incorrect Username or Password";
                                        }
                                        ?></h5></center>
                            </div>




                            <div class="checkbox">
                                <label>
                                    <input type="checkbox"> Remember Me
                                </label>
                                <label class="pull-right">
                                    <a href="forgot_admin">Forgot Password?</a>
                                </label>

                            </div>
                            <button type="submit" value="Login" class="btn btn-secondary btn-flat m-b-30 m-t-30" name="btnlogin">Sign in</button>


                        </form>
                    </div>
                </div>
            </div>
        </div>

        <footer class="site-footer">
            <div class="footer-inner bg-white">
                <div class="row">
                    <div class="col-sm-6">
                        Copyright &copy; <?php echo date("Y") ?> The Jewellary
                    </div>
                    <div class="col-sm-6 text-right">
                        Designed by Simex ltd.
                    </div>
                </div>
            </div>
        </footer>
        <script src="assets/js/vendor/jquery-2.1.4.min.js"></script>
        <script src="assets/js/popper.min.js"></script>
        <script src="assets/js/plugins.js"></script>
        <script src="assets/js/main.js"></script>


    </body>
</html>
<?php
ob_end_flush();
?>

