<?php
ob_start();
session_start();

include_once './connection.php';

if (isset($_SESSION['suname_admin'])) {
    
} else {
    header('Location: admin_login.php');
}

if (isset($_POST['btndsearch'])) {
    $vieworder = "SELECT tbl_customer.fname,tbl_payment.*,tbl_customer_order.product_id,quantity FROM tbl_payment INNER JOIN tbl_customer ON tbl_customer.customer_id=tbl_payment.customer_id INNER JOIN tbl_customer_order ON tbl_customer_order.customer_order_id=tbl_payment.order_id WHERE tbl_payment.date BETWEEN '" . $_POST['fdate'] . "' AND '" . $_POST['tdate'] . "' GROUP BY tbl_customer_order.customer_order_id";
} else {

//$vieworder="SELECT tbl_customer_order.address,bill_id,customer_order_id,total_amount,tbl_payment.*,tbl_customer.fname,mname,lname FROM tbl_customer_order INNER JOIN tbl_payment ON tbl_payment.order_id=tbl_customer_order.customer_order_id INNER JOIN tbl_customer ON tbl_customer.customer_id=tbl_customer_order.customer_id";
    $vieworder = "SELECT tbl_customer.fname,tbl_payment.*,tbl_customer_order.product_id,quantity FROM tbl_payment INNER JOIN tbl_customer ON tbl_customer.customer_id=tbl_payment.customer_id INNER JOIN tbl_customer_order ON tbl_customer_order.customer_order_id=tbl_payment.order_id GROUP BY tbl_customer_order.customer_order_id";
}
$rsorder = mysqli_query($con, $vieworder);
$av = "";
$dataorder = "";
$ro = "";
if (mysqli_num_rows($rsorder) > 0) {
//$av=1;

    while ($roworder = mysqli_fetch_assoc($rsorder)) {

        $ro = $roworder['order_id'];
        $dataorder .= "<tr>";
        $dataorder .= "<td>";
        $dataorder .= "<a href='javascript:void(0);' style='color:blue;' data-toggle='modal' data-target='#order_" . $roworder['order_id'] . "' >" . $roworder['order_id'] . "</a>";
        $dataorder .= "</td>";

        $dataorder .= "<td>";
        $dataorder .= ucfirst($roworder['txnid']);
        $dataorder .= "</td>";

        $dataorder .= "<td>";
        $dataorder .= ucfirst($roworder['fname']);
        $dataorder .= "</td>";

        $dataorder .= "<td>";
        $dataorder .= ucfirst($roworder['product_id']);
        $dataorder .= "</td>";

        $dataorder .= "<td>";
        $dataorder .= ucfirst($roworder['quantity']);
        $dataorder .= "</td>";

        $dataorder .= "<td>";
        $dataorder .= ucfirst($roworder['amount']);
        $dataorder .= "</td>";

        $dataorder .= "<td>";
        $dataorder .= ucfirst($roworder['get_way_name']);
        $dataorder .= "</td>";

        $dataorder .= "<td>";
        $dataorder .= ucfirst($roworder['status']);
        $dataorder .= "</td>";

        $dataorder .= "<td>";
        $dataorder .= ucfirst($roworder['date']);
        $dataorder .= "</td>";



        $dataorder .= "</tr>";
    }
}
?>



<html>
    <head>
        <title>The Jewellary</title>
        <?php
        include('head.php');
        ?>



    </head>
    <body>
        <?php
        include('left.php');
        ?>

        <!-- Right Panel --> 
        <div id="right-panel" class="right-panel">
            <?php
            include('header.php');
            ?>

            <div class="content pb-0"> 

                <section>


                    <?php
                    $order = "SELECT tbl_customer.fname,tbl_payment.*,tbl_customer_order.product_id,quantity,bill_id FROM tbl_payment INNER JOIN tbl_customer ON tbl_customer.customer_id=tbl_payment.customer_id INNER JOIN tbl_customer_order ON tbl_customer_order.customer_order_id=tbl_payment.order_id";

                    $rsod = mysqli_query($con, $order);
                    if (mysqli_num_rows($rsod) > 0) {
                        while ($rowmodal = mysqli_fetch_assoc($rsod)) {
                            ?>
                            <!-- The Modal -->
                            <div class="modal fade" id="order_<?php echo $rowmodal['order_id'] ?>">
                                <div class="modal-dialog modal-xl" style="max-width: 90%;">
                                    <div class="modal-content">

                                        <!-- Modal Header -->
                                        <div class="modal-header">
                                            <h4 class="modal-title">Modal Heading</h4>
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        </div>

                                        <!-- Modal body -->
                                        <div class="modal-body">
                                            <?php
                                            $datamodal = "";
                                            // $sql2 = "select * from tbl_customer_order where customer_order_id='" . $rowmodal['order_id'] . "'";
                                            //$sql2 = "SELECT tbl_customer_order.*,tbl_customer.fname,mname,lname FROM tbl_customer_order INNER JOIN tbl_customer ON tbl_customer.customer_id=tbl_customer_order.customer_id WHERE tbl_customer_order.customer_order_id='" . $rowmodal['order_id'] . "'";
                                            $sql2 = "SELECT tbl_customer_order.*,tbl_customer.fname,mname,lname,tbl_city.city_name,tbl_product.model_no FROM tbl_customer_order INNER JOIN tbl_customer ON tbl_customer.customer_id=tbl_customer_order.customer_id INNER JOIN tbl_city ON tbl_city.city_id=tbl_customer_order.city_id INNER JOIN tbl_product ON tbl_product.model_no=tbl_customer_order.product_id WHERE tbl_customer_order.customer_order_id='" . $rowmodal['order_id'] . "'";
                                            $res2 = mysqli_query($con, $sql2);

                                            if (mysqli_num_rows($res2)) {
                                                while ($row2 = mysqli_fetch_assoc($res2)) {
                                                    $datamodal .= "<tr>";
                                                    $datamodal .= "<td>";
                                                    $datamodal .= $row2['bill_id'];
                                                    $datamodal .= "</td>";

                                                    $datamodal .= "<td>";
                                                    $datamodal .= $row2['customer_order_id'];
                                                    $datamodal .= "</td>";

                                                    $datamodal .= "<td>";
                                                    $datamodal .= ucfirst($row2['fname']);
                                                    $datamodal .= "</td>";
                                                    $datamodal .= "<td>";
                                                    $datamodal .= ucfirst($row2['address']);
                                                    $datamodal .= "</td>";

                                                    $datamodal .= "<td>";
                                                    $datamodal .= ucfirst($row2['city_name']);
                                                    $datamodal .= "</td>";

                                                    $datamodal .= "<td>";
                                                    $datamodal .= ucfirst($row2['model_no']);
                                                    $datamodal .= "</td>";

                                                    $datamodal .= "<td>";
                                                    $datamodal .= ucfirst($row2['quantity']);
                                                    $datamodal .= "</td>";

                                                    $datamodal .= "<td>";
                                                    $datamodal .= ucfirst($row2['total_amount']);
                                                    $datamodal .= "</td>";

                                                    $datamodal .= "<td>";
                                                    $datamodal .= ucfirst($row2['date']);
                                                    $datamodal .= "</td>";

                                                    $datamodal .= "<td>";
                                                    $datamodal .= "<a href='javascript:void(0);'><i class='fa fa-file-pdf-o' style='font-size:24px;color:red;'></a>";
                                                    $datamodal .= "</td>";


                                                    $datamodal .= "</tr>";
                                                }
                                            } else {
                                                echo "Record Not Found";
                                            }
                                            ?>


                                            <div class="orders">
                                                <div class="row">
                                                    <div class="col-xl-12"> 
                                                        <div class="card">
                                                            <div class="card-body">
                                                                <h4 class="box-title">Order Details </h4>
                                                            </div>

                                                            <div class="card-body--">

                                                                <div class="table-stats order-table ov-h">
                                                                    <table class="table table-hover table-bordered">
                                                                        <thead>
                                                                            <tr>
                                                                              <!--  <th class="serial">#</th> -->
                                                                                <th>Bill id</th>
                                                                                <th>Order id</th>
                                                                                <th>Customer name</th>
                                                                                <th>Address</th>
                                                                                <th>City</th>
                                                                                <th>Product</th>
                                                                                <th>Quantity</th>
                                                                                <th>Total amount</th>
                                                                                <th>Date</th>
                                                                                <th>Bill Report</th>


                                                                            </tr>
                                                                        </thead>
                                                                        <tbody> 
                                                                            <?php
                                                                            echo $datamodal;
                                                                            ?>
                                                                        </tbody>
                                                                    </table>
                                                                </div> <!-- /.table-stats -->
                                                            </div>
                                                        </div> <!-- /.card -->
                                                    </div>  <!-- /.col-lg-8 -->


                                                </div> 
                                            </div> <!-- /.order -->
                                        </div>

                                        <!-- Modal footer -->
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        </div>

                                    </div>
                                </div>
                            </div>


                            <?php
                        }
                    }
                    ?>


                    <div class="content">
                        <div class="animated fadeIn">
                            <div class="row">

                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <strong class="card-title"><b style="color: #0087ba;">Customer order Payments</b></strong>
                                        </div>

                                        <div class="card-body">
                                            <form action="#" method="post">
                                                <div class="form-inline">
                                                    <input type="date" name="fdate" class="form-control col-md-2" required="">&nbsp;&nbsp;
                                                    <input type="date" name="tdate" class="form-control col-md-2" required="">&nbsp;&nbsp;
                                                    <button type="submit" name="btndsearch" class="btn btn-success"><i class="fa fa-search"></i></button>

                                                </div>

                                            </form>

                                            <table id="bootstrap-data-table" style="width: 100px !important; " class="table table-hover table-bordered">

                                                <thead>
                                                    <tr>
                                                        <th>Order id</th>
                                                        <th>transaction id</th>
                                                        <th>Customer Name</th>
                                                        <th>Product Model</th>
                                                        <th>Quantity</th>
                                                        <th>Total amount</th>
                                                        <th>Gateway</th>
                                                        <th>payment Status</th>
                                                        <th>Payment Date</th>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    echo $dataorder;
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div><!-- .animated -->
                    </div><!-- .content -->




                </section>


            </div>



            <div class="clearfix"></div>

            <?php
            include ('footer.php');
            ?>

        </div>
        <?php
        include('script.php');
        ?>
        <div id="container">



        </div>

    </body>

</html>
