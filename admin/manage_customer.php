<?php
ob_start();
session_start();

$data = "";
$x = "";
$st = "";

$curdate = date('Y-m-d');
if (isset($_SESSION['suname_admin'])) {
    
} else {
    header('Location: admin_login.php');
}


include_once './connection.php';

if (isset($_POST['btnsearchdate'])) {

    $selectview = "select c.customer_id,c.fname,c.status ,c.add_on,c.mname,c.lname,c.address,c.gender,c.contactno,c.email, ci.city_name from tbl_customer as c INNER JOIN tbl_city as ci ON ci.city_id=c.city_id where DATE(c.add_on) BETWEEN '" . $_POST['fdate'] . "' AND '" . $_POST['tdate'] . "'order by c.add_on DESC";
} else {
    $selectview = "select c.customer_id,c.fname,c.status ,c.add_on,c.mname,c.lname,c.address,c.gender,c.contactno,c.email, ci.city_name from tbl_customer as c INNER JOIN tbl_city as ci ON ci.city_id=c.city_id order by c.add_on DESC";
}
$resultcu = mysqli_query($con, $selectview);

if (mysqli_num_rows($resultcu) > 0) {
    $x = 1;
    while ($row = mysqli_fetch_assoc($resultcu)) {

        $ad = $row['add_on'];

        if ($ad == $curdate) {
            $data .= "<tr id='trid_" . $row['customer_id'] . "' class='table-success'>";
        }

        /*  $data .= "<td>";
          $data .= "<input type='checkbox' name='cbdelete[]' onchange='fncb()'; class='cbid'  value='" . $row['customer_id'] . "' >";
          $data .= "</td>"; */



        $data .= "<td>";
        $f = $row['fname'];
        $m = $row['mname'];
        $l = $row['lname'];

        $fullname = $f . " " . $m . " " . $l;

        $data .= "<div class='of" . $row['customer_id'] . "'>" . ucfirst($fullname) . "</div>";
        $data .= "<input type='text' name='txtename' value='$fullname' style='display:none;' class='edittext" . $row['customer_id'] . " txtename" . $row['customer_id'] . "' >";
        $data .= "</td>";

        $data .= "<td>";
        $data .= "<div class='of" . $row['customer_id'] . "'>" . ucfirst($row['address']) . "</div>";
        $data .= "<input type='text' name='txteaddress' value='" . $row['address'] . "' style='display:none;' class='edittext" . $row['customer_id'] . " txteaddress" . $row['customer_id'] . "'>";
        $data .= "</td>";

        $data .= "<td>";
        $bb = "--Select City--";
        $b = "";
        $data .= "<div class='of" . $row['customer_id'] . "'>" . ucfirst($row['city_name']) . "</div>";
        $viewcity = "SELECT * FROM tbl_city";
        $rscity = mysqli_query($con, $viewcity);
        $data .= "<select name='ecity' required class='edittext" . $row['customer_id'] . " ecity" . $row['customer_id'] . " ' style='display:none;'>";
        if ($rscity) {
            $data .= "<option value = " . $b . ">" . $bb . "</option>";
            while ($rowcity1 = mysqli_fetch_array($rscity)) {
                $city = ucfirst($rowcity1['city_name']);
                $id = $rowcity1['city_id'];

                $data .= "<option value=" . $id . " >" . $city . "</option>";
            }
        }
        $data .= "</select>";
        $data .= "</td>";

        $data .= "<td>";

        $check = $row['gender'];
        if ($check == 'm') {
            $ge = "Male";
        } else {

            $ge = "Female";
        }
        $data .= "<div class='of" . $row['customer_id'] . "'>" . ucfirst($ge) . "</div>";
//$data .= "<input type='text' name='txtegender' value='" . $ge . "' style='display:none;' class='edittext" . $row['customer_id'] . " txtegender" . $row['customer_id'] . " '>";
        if ($check == 'm') {
            $data .= "<div style='display:none;' class='edittext" . $row['customer_id'] . " txtegender" . $row['customer_id'] . " '>Male<input type='radio' value='m' name='txtegender' checked='' style='display:none;' class='edittext" . $row['customer_id'] . " txtegender" . $row['customer_id'] . " '></div>";
            $data .= "<div style='display:none;' class='edittext" . $row['customer_id'] . " txtegender" . $row['customer_id'] . " '>Female<input type='radio' value='f' name='txtegender'  style='display:none;' class='edittext" . $row['customer_id'] . " txtegender" . $row['customer_id'] . " '></div>";
        } else {

            $data .= "<div style='display:none;' class='edittext" . $row['customer_id'] . " txtegender" . $row['customer_id'] . " '>Male<input type='radio' value='m' name='txtegender' style='display:none;' class='edittext" . $row['customer_id'] . " txtegender" . $row['customer_id'] . " '></div>";
            $data .= "<div style='display:none;' class='edittext" . $row['customer_id'] . " txtegender" . $row['customer_id'] . " '>Female<input type='radio' value='f' name='txtegender' checked='' style='display:none;' class='edittext" . $row['customer_id'] . " txtegender" . $row['customer_id'] . " '></div>";
        }
        $data .= "</td>";

        $data .= "<td>";
        $data .= "<div class='of" . $row['customer_id'] . "'>" . ucfirst($row['contactno']) . "</div>";
        $data .= "<input type='text' name='txtecontactno' value='" . $row['contactno'] . "' style='display:none;' class='edittext" . $row['customer_id'] . " txtecontactno" . $row['customer_id'] . " '>";
        $data .= "</td>";

        $data .= "<td>";
        $data .= "<div class='of" . $row['customer_id'] . "'>" . $row['email'] . "</div>";
        $data .= "<input type='text' name='txteemail' value='" . $row['email'] . "' style='display:none;' class='edittext" . $row['customer_id'] . " txteemail" . $row['customer_id'] . "  ' disabled>";
        $data .= "</td>";

        $data .= "<td>";
        if ($row['status'] == 1) {
//$data .= "<a href='manage_customer.php?cid=" . $row['customer_id'] . "&sid=" . $row['status'] . " &email=" . $row['email'] . "'  style='color:green;'><i class='fa fa-toggle-on'></i></a>&nbsp;&nbsp;";
            $data .= "<div id='togle11" . $row['customer_id'] . "'><a href='javascript:void(0);' class='action" . $row['customer_id'] . "' onclick='fnactive(&quot;" . $row['email'] . "&quot;," . $row['customer_id'] . "," . $row['status'] . ");'  style='color:green;'><i class='fa fa-toggle-on'></i></a></div>&nbsp;&nbsp;";
        } else {

//$data .= "<a href='manage_customer.php?cid=" . $row['customer_id'] . "&sid=" . $row['status'] . " &email=" . $row['email'] . "'  style='color:red;' ><i class='fa fa-toggle-off'></i></a>&nbsp;&nbsp;&nbsp;";
            $data .= "<div id='togle11" . $row['customer_id'] . "'><a href='javascript:void(0);' class='action" . $row['customer_id'] . "'  onclick='fnactive(&quot;" . $row['email'] . "&quot;," . $row['customer_id'] . "," . $row['status'] . ");'  style='color:orange;'><i class='fa fa-toggle-off'></i></a></div>&nbsp;&nbsp;";
        }
        $data .= "</td>";

        $data .= "<td>";
        $data .= "<a href='javascript:void(0);' class='action" . $row['customer_id'] . "'  onclick='fnedit(" . $row['customer_id'] . ");' style=color:blue;><i class='fa fa-edit'></i></a>&nbsp;&nbsp;";
        //$data .= "<a href='javascript:void(0);' style=color:red; class='action" . $row['customer_id'] . "'  onclick='fndelete(" . $row['customer_id'] . ");'><i class='fa fa-trash'></i></a>";
        $data .= "<input type='button' name='btnupdate' onclick='fnupdate(" . $row['customer_id'] . ");'  value='Update' style='display:none;' class='btn btn-success edittext" . $row['customer_id'] . "'>";
        $data .= "<input type='button' name='btnremove' value='Cancel' style='display:none;' class='btn btn-danger edittext" . $row['customer_id'] . "'>";
        $data .= "</td>";



        $data .= "</tr>";
    }
} else {
   /* $data .= "<tr>";
    $data .= "<td>";
    $data .= "<center>No Records Found!!</center>";
    $data .= "</td>";


    $data .= "</tr>";*/
}



if (isset($_POST['btnok'])) {
    foreach ($_POST['cbdelete'] as $value) {

        $mdelete = "DELETE FROM tbl_customer WHERE customer_id='" . $value . "' ";

        if (mysqli_query($con, $mdelete)) {
            header('Location:manage_customer.php');
        } else {
            echo "not";
        }
    }
}
?>



<html>  
    <head>
        <title>The Jewellary</title>
        <?php
        include('head.php');
        ?>

    <body>
        <?php
        include('left.php');
        ?>

        <!-- Right Panel --> 
        <div id="right-panel" class="right-panel">
            <?php
            include('header.php');
            ?>

            <div class="content pb-0"> 

                <section>


                    <div class="content">
                        <div class="animated fadeIn">
                            <div class="row">

                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-header col-12 col-md-12">
                                            <div class="row" >
                                                <div class="col-lg-8"><strong class="card-title col-md-8">Customer Details</strong></div>

                                                <div class="col-lg-4"><a href="new_customer.php" id="btnnc" class="btn btn-primary pull-right"><i class=" fa fa-plus">  New Customer </i></a></div>
                                            </div>
                                        </div>


                                        <div class="card-body">
                                            <form action="#" method="post">
                                                <div class="form-inline">
                                                    <input type="date" name="fdate" class="form-control col-md-2" required="">&nbsp;&nbsp;
                                                    <input type="date" name="tdate" class="form-control col-md-2" required="">&nbsp;&nbsp;<button type="submit" name="btnsearchdate" class="btn btn-success "><i class="fa fa-search"></i></button>
                                                </div>
                                            </form>
                                            <table id="bootstrap-data-table" class="table table-striped table-bordered">


                                                <thead>
                                                    <tr>
                                                         <!--   <th>Select</th>  -->
                                                        <th>Name</th>
                                                        <th>Address</th>
                                                        <th>City</th>
                                                        <th>Gender</th>
                                                        <th>Contactno</th>
                                                        <th>E-mail</th>
                                                        <th>Status</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    echo $data;
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div><!-- .animated -->
                    </div><!-- .content -->


                </section>


            </div>



            <div class="clearfix"></div>

            <?php
            include ('footer.php');
            ?>

        </div>
        <?php
        include('script.php');
        ?>
        <div id="container">



        </div>



        <script>



            function fnclick(id)
            {
                $(".vtr_" + id).show();
                //alert('hi');
            }


            function fncb()
            {

                //alert('hi');

                if ($(".cbid").is(':checked'))
                {
                    $(".btnok").show();
                    //$(".btnok").attr("disabled",false);
                }
                if (!$(".cbid").is(':checked'))
                {

                    $(".btnok").hide();
                    //$(".btnok").attr("disabled",true);
                }
            }


        </script>





        <script type="text/javascript">
            $(document).ready(function () {
                $('#tblid').DataTable();
            });
        </script>




    </body>

</html>
