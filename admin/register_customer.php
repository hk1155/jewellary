<html lang="en">
    <head>
        <title>Register</title>
        <?php include('Head_customer.php') ?>

    </head>
    <style> 
        .bg1{
            background:url(bg.jpg); background-size: cover; width: 100%; height: 100%;  
        }
    </style>
    <body class="animsition">

        <header class="header-v3">
            <?php include('Header_customer.php') ?>
        </header>
        <?php include('Cart.php') ?>
        <div style="clear:both;"></div>
        <div  class="bg1">
            <div class="container">
                <div class="row">
                    <div class="col-sm-7"></div>

                    <div class="col-sm-5" style="position: relative; top: 60px;">
                        <form action="OTPpage.php" method="post" id="form1">
                            <div class="login-form">
                                <h6 class="mtext-105 cl2 txt-center p-b-30" style="font-size: 40px; color:black; margin-bottom: 0px; padding: 0px 0;">
                                    <b><u>Register</u></b>
                                </h6><br>
                                <div class="row form-group">
                                    <div class="col col-md-12">
                                        <div class="input-group">
                                            <input type="text" id="fname" name="txtfname" class="form-control" placeholder="First Name" required pattern="^[A-Za-z]{3,50}$" title="Please Enter Valid Name">
                                            &nbsp;<input type="text" id="mname" name="txtmname" class="form-control" placeholder="Middle Name" required  pattern="^[A-Za-z]{3,50}$" title="Please Enter Valid Name">
                                            &nbsp;<input type="text" id="lname" name="txtlname" minlength="3" class="form-control" placeholder="Last Name" required  pattern="^[A-Za-z]{3,50}$" title="Please Enter Valid Name">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <textarea type="text" id="address" name="txtaddress" placeholder="Address" class="form-control" required pattern="^[A-Za-z]{3,150}$" title="Please Enter Valid Address" ></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                       <!-- <select name="txtcity" class="form-control">
                                            <option value="" >-- Select City --</option>
                                            <option value="surat">Surat</option>
                                            <option value="bardoli">Bardoli</option>
                                        </select> -->
                                        <select name="txtcity"  class="form-control" >
                                                    <option value="">----select city ----</option>
                                                    <?php
                                                $servername = "localhost";
                                                $username = "root";
                                                $password = "Hk1463";
                                                $dbname = "jewellary";

                                                $conn = new mysqli($servername, $username, $password, $dbname);

                                                
                                                $sql1 = "select * from tbl_city";
                                                $res1 = mysqli_query($conn, $sql1);


                                                if (mysqli_num_rows($res1)) {
                                                   while($row= mysqli_fetch_assoc($res1))
                                                   { ?>
                                                    <option value="<?php echo $row['city_id'] ?>"><?php echo $row['city_name'] ?></option> 
                                                    
                                                  <?php
                                                   }
                                                } else {
                                                    echo "Data not Found";
                                                }
                                                ?>

                                                </select>
                                        
                                        
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">

                                        Gender :&nbsp; <input type="radio" name="gender"  value="m" Selected>&nbsp;Male&nbsp;
                                        &nbsp;<input type="radio" name="gender"  value="f">&nbsp;Female

                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" id="contact" name="txtcontact" placeholder="Contact number" class="form-control" maxlength="10" minlength="10" onkeypress='return event.charCode >= 48 && event.charCode <= 57' pattern="[7986][0-9]{9}" title="Please Enter Valid Mobile Number" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="email" id="email" name="txtemail" placeholder="Email" class="form-control" required pattern="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" title="Please Enter Valid Email Address">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="password" id="password" name="pw1" onkeyup='check();' placeholder="Password" class="form-control" pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="password" id="confirm_password" onkeyup='check();' name="pw2" placeholder="Conform Password" class="form-control" required> 
                                    </div>
                                    <span id='message'></span>
                                </div>
                                <input type="submit" class="flex-c-m stext-101 cl0 size-121 bg3 bor1 hov-btn3 p-lr-15 trans-04 pointer"  style="width: 100%;" name="reg" value="Submit" >

                            </div>
                        </form>                   
                    </div>
                </div>
            </div>
            <div  style="width:36%;  margin-left: 750px;  ">
            </div>
        </div>
        <footer class="bg3 p-t-75 p-b-32">
            <?php include('Footer_customer.php') ?>
        </footer>
        <?php include('js_customer.php') ?>
    </body>
</html>
<script>
    $(document).ready(function () {

        $(".hd").hide();

        $("#hd1").click(function () {

            $(".hd").show();
        });

    });
</script>
<script>
    var check = function () {
        if (document.getElementById('password').value ==
                document.getElementById('confirm_password').value) {
            document.getElementById('message').style.color = 'green';
            document.getElementById('message').innerHTML = 'Conform Password is matching to password';
        } else {
            document.getElementById('message').style.color = 'red';
            document.getElementById('message').innerHTML = 'Conform Password is not matching to password';
        }
    }
</script>