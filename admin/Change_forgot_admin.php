<?php
ob_start();
include_once './connection.php';

if (isset($_GET['em1'])) {
    $email1 = $_GET['em1'];
    
    base64_decode($email1);
}
?>
<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Admin Login</title>
        <meta name="description" content="Ela Admin - HTML5 Admin Template">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="images/alogo.png">
        <link rel="shortcut icon" href="images/halogo.png"> 

        <link rel="stylesheet" href="assets/css/normalize.css">
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/css/themify-icons.css">
        <link rel="stylesheet" href="assets/css/pe-icon-7-filled.css">
        <link rel="stylesheet" href="assets/css/flag-icon.min.css">
        <link rel="stylesheet" href="assets/css/cs-skin-elastic.css">
        <!-- <link rel="stylesheet" href="assets/css/bootstrap-select.less"> -->
        <link rel="stylesheet" href="assets/css/style.css">

        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

    <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script> -->

    </head>
    <body class="bg-light">


        <div class="sufee-login d-flex align-content-center flex-wrap ">
            <div class="container">
                <div class="login-content col-lg-2">

                    <div class="login-form">
                        <form action="#" method="post">

                            <div class="login-logo">
                                <a href="#">
                                    <img class="align-content" src="images/logo11.png" alt="images/logo11.png">
                                </a>
                            </div>


                            <div class="row form-group">
                                <div class="col col-md-12">
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-asterisk"></i></div>
                                        <input type="password" id="txtpassword" onkeyup='check();'  name="txtpassword" class="form-control" placeholder="Enter New Password" required="required">
                                    </div>
                                </div>
                            </div>



                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-asterisk"></i></div>
                                    <input type="password" id="txtcpassword" onkeyup='check();'  name="txtcpassword" placeholder="Conform Password" class="form-control" required="required">

                                </div>
                                <span id='message'></span>
                                <center>
                                    <?php
                                    if (isset($_POST['btnchangepwd'])) {
                                        $password = md5($_POST['txtpassword']);
                                        $cpassword = md5($_POST['txtcpassword']);

                                        if ($password == $cpassword) {
                                            $change = "UPDATE tbl_login SET password='" . $cpassword . "' WHERE username='" . base64_decode($email1) . "'";
                                            $res = mysqli_query($con, $change);
                                            if ($res) {
                                                echo "Successfully";
                                                header('Location:index.php');
                                            }
                                            else
                                            {
                                                echo "Not Successfully";
                                            }
                                            
                                        } 
                                    }
                                    ?>
                                </center>


                            </div>




                            <button type="submit" value="Change Password" class="btn btn-success btn-flat m-b-30 m-t-30" name="btnchangepwd">Change password</button>


                        </form>
                    </div>
                </div>
            </div>
        </div>

        <footer class="site-footer">
            <div class="footer-inner bg-white">
                <div class="row">
                    <div class="col-sm-6">
                        Copyright &copy; <?php echo date("Y") ?> The Jewellary
                    </div>
                    <div class="col-sm-6 text-right">
                        Designed by Simex ltd.
                    </div>
                </div>
            </div>
        </footer>
        <script src="assets/js/vendor/jquery-2.1.4.min.js"></script>
        <script src="assets/js/popper.min.js"></script>
        <script src="assets/js/plugins.js"></script>
        <script src="assets/js/main.js"></script>

        <script>
                                        var check = function () {
                                            if (document.getElementById('txtpassword').value ==
                                                    document.getElementById('txtcpassword').value) {
                                                document.getElementById('message').style.color = 'green';
                                                document.getElementById('message').innerHTML = 'Conform Password is matching to password';
                                            } else {
                                                document.getElementById('message').style.color = 'red';
                                                document.getElementById('message').innerHTML = 'Conform Password is not matching to password';
                                            }
                                        }
        </script>
    </body>
</html>


