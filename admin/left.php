
<!-- Left Panel --> 
<aside id="left-panel" class="left-panel">
    <nav class="navbar navbar-expand-sm navbar-default"> 
        <div id="main-menu" class="main-menu collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active">
                    <a href="admin_home.php"><i class="menu-icon fa fa-laptop"></i>Dashboard </a>
                </li>
                <li class="menu-title">General</li><!-- /.menu-title -->

                <li class="">
                    <a href="manage_customer.php"><i class="menu-icon fa fa-user"></i>Manage Customers </a>
                </li>
                <li class="">
                    <a href="manage_daily_price.php"><i class="menu-icon fa fa-rupee"></i>Material Prices </a>
                </li>

                <li class="">
                    <a href="manage_purity.php"><i class="menu-icon fa fa-cart-plus"></i>Manage Category </a>
                </li>
                <li class="">
                    <a href="View_products.php"><i class="menu-icon fa fa-product-hunt"></i>Manage Products </a>
                </li>




                <li class="menu-title">Customers</li><!-- /.menu-title -->
                <li>
                    <a href="customer_req.php"> <i class="menu-icon fa fa-registered"></i>Requirements  </a>

                </li>
                <li>
                    <a href="customer_orders_a.php"> <i class="menu-icon fa fa-first-order"></i>Customer Orders  </a>

                </li>



                <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-diamond"></i>Manage Purity</a>
                    <ul class="sub-menu children dropdown-menu">
                  <!--      <li><i class="menu-icon fa-plus-square-o"></i><a href="font-fontawesome.html">Add Item</a></li>
                        <li><i class="menu-icon ti-themify-logo"></i><a href="font-themify.html">View All Items</a></li> -->
                    </ul>
                </li>
                <li>
                    <a href="reports.php"> <i class="menu-icon fa fa-pie-chart"></i>Reports
                    </a>
                </li>




                <li class="menu-title">Extras</li><!-- /.menu-title -->
                <!-- <li class="menu-item-has-children dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-glass"></i>Pages</a>
                     <ul class="sub-menu children dropdown-menu">
                         <li><i class="menu-icon fa fa-sign-in"></i><a href="page-login.html">Login</a></li>
                         <li><i class="menu-icon fa fa-sign-in"></i><a href="page-register.html">Register</a></li>
                         <li><i class="menu-icon fa fa-paper-plane"></i><a href="pages-forget.html">Forget Pass</a></li>
                     </ul>
                 </li> -->
                <li>
                    <a href="metal_history.php"> <i class="menu-icon fa fa-newspaper-o"></i>Metal History
                    </a>
                </li>
                
                <li>
                    <a href="#"> <i class="menu-icon fa fa-newspaper-o"></i>Manage Feedback
                    </a>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </nav>
</aside><!-- /#left-panel --> 
<!-- Left Panel -->