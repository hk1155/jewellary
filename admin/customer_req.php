<?php
ob_start();
session_start();

if (isset($_SESSION['suname_admin'])) {
    
} else {
    header('Location: index.php');
}

include_once './connection.php';
$curdate = date('Y-m-d');


$view = "SELECT tbl_customize_product.*,tbl_customer.fname,lname,tbl_pcategory_type.*,tbl_pcategory.name,tbl_material_type.type FROM tbl_customize_product INNER JOIN tbl_customer ON tbl_customize_product.customer_id=tbl_customer.customer_id INNER JOIN tbl_pcategory_type ON tbl_pcategory_type.cat_type_id=tbl_customize_product.pctid INNER JOIN tbl_pcategory ON tbl_pcategory.category_id=tbl_pcategory_type.cat_id INNER JOIN tbl_material_type ON tbl_material_type.mtype_id=tbl_pcategory_type.matel_id ORDER BY add_on DESC";

$resultview = mysqli_query($con, $view);

$av = "";
$dataview = "";
if (mysqli_num_rows($resultview) > 0) {
    $av = 1;

    while ($rowdata = mysqli_fetch_assoc($resultview)) {

        $ao = $rowdata['add_on'];
        $st1 = $rowdata['status'];


        $fname = $rowdata['fname'];
        $lname = $rowdata['lname'];

        $fullname = $fname . " " . $lname;

        /* if ($ao == $curdate) {


          $dataview .= "<tr id='trid_" . $rowdata['customer_id'] . "' class='table-success'>";
          } */
        if ($st1 == 'pending') {
            $dataview .= "<tr id='trid_" . $rowdata['customer_id'] . "' class='table-danger'>";
        } elseif ($ao == $curdate) {
            $dataview .= "<tr id='trid_" . $rowdata['customer_id'] . "' class='table-success'>";
        }
         elseif ($ao == $curdate && $st1=='pending') {
            $dataview .= "<tr id='trid_" . $rowdata['customer_id'] . "' class='table-primary'>";
        }
        


        $dataview .= "<td>";
        $dataview .= "<img src='" . $rowdata['image'] . "' onclick='showImage(" . $rowdata['image'] . ");' class='rounded'>";
        $dataview .= "</td>";

        $dataview .= "<td>";

        $da = $rowdata['customer_id'];
        //$dataview .= ucfirst($fullname);
        $dataview .= "<a href='javascript:void(0);' data-toggle='modal' data-target='#c_" . $rowdata['customer_id'] . "' title='Customer Information'  style='color:black;'>" . ucfirst($fullname) . "</a>";
        $dataview .= "</td>";

        $dataview .= "<td>";
        $dataview .= ucfirst($rowdata['purity_id']);
        $dataview .= "</td>";

        $dataview .= "<td>";
        $dataview .= ucfirst($rowdata['type']);
        $dataview .= "</td>";

        $dataview .= "<td>";
        $dataview .= ucfirst($rowdata['name']);
        $dataview .= "</td>";

        $dataview .= "<td>";
        $dataview .= ucfirst($rowdata['quantity']);
        $dataview .= "</td>";

        $dataview .= "<td>";
        $dataview .= ucfirst($rowdata['weight']);
        $dataview .= "</td>";

        $dataview .= "<td>";
        $dataview .= ucfirst($rowdata['price']);
        $dataview .= "</td>";
        
        $dataview .= "<td>";
        $dataview .= ucfirst($rowdata['add_on']);
        $dataview .= "</td>";
        
        $dataview .= "<td>";
        if ($rowdata['status'] == 'pending') {

            $dataview .= " <div id='sta" . $rowdata['cupid'] . "' style='color: orangered;'>" . ucfirst($rowdata['status']) . "</div>";
        } else {

            $dataview .= " <div id='sta" . $rowdata['cupid'] . "'>" . ucfirst($rowdata['status']) . "</div>";
        }
        $dataview .= "</td>";

        $dataview .= "<td>";
        $dataview .= "<center>";
        if ($rowdata['status'] == 'Accepted') {
            $dataview .= "<B style='color:#008c69'>Conform</B>";
        } elseif ($rowdata['status'] == 'Rejected') {
            $dataview .= "<B style='color:#DC143C'>Cancel</B>";
        } else {

            $dataview .= "<a href='javascript:void(0);'id='cc' class='true' onclick='fncuz1(" . $rowdata['cupid'] . ");'  style='margin-right:20px;'><i class='fa fa-check' style='color:green;'></i></a>";

            $dataview .= "<a href='javascript:void(0);' onclick='fnreject1(" . $rowdata['cupid'] . ");'  style='margin-right:20px;'><i class='fa fa-close' style='color:red;'></i></a>";
        }

        $dataview .= "</center>";

        $dataview .= "</td>";

        $dataview .= "</tr>";
    }
} else {

    /* $av = 0;
      $dataview .= "<tr>";
      $dataview .= "<td>";
      $dataview .= "No Records Found!!";
      $dataview .= "</td>";
      $dataview .= "</tr>"; */
}
?>

<html>
    <head>
        <title>The Jewellary</title>
        <?php
        include('head.php');
        ?>



    </head>
    <body>
        <?php
        include('left.php');
        ?>

        <!-- Right Panel --> 
        <div id="right-panel" class="right-panel">
            <?php
            include('header.php');
            ?>

            <div class="content pb-0"> 

                <section>


                    <?php
                    $sql = "select *from tbl_customer";
                    $res = mysqli_query($con, $sql);

                    if (mysqli_num_rows($res)) {
                        while ($row = mysqli_fetch_assoc($res)) {
                            ?>


                            <div class="modal fade" id="c_<?php echo $row['customer_id'] ?>">
                                <div class="modal-dialog">
                                    <div class="modal-content">

                                        <!-- Modal Header -->
                                        <div class="modal-header">
                                            <h5 class="modal-title" style="color: #0087ba;"><u>Customer Details</u></h5>
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>

                                        </div>



                                        <!-- Modal body -->
                                        <div class="modal-body" style="background-color: lightyellow;">
                                            <div class="table-responsive">
                                                <table class="table-bordered">

                                                    <tr>  
                                                        <td width="40%">Name</td>  
                                                        <?php
                                                        $fullname = $row['fname'] . " " . $row['mname'] . " " . $row['lname'];
                                                        ?>
                                                        <td width="80%"><b><?php echo ucfirst($fullname); ?></b></td>  
                                                    </tr> 
                                                    <tr>  
                                                        <td width="30%">Gender</td> 
                                                        <?php
                                                        if ($row['gender'] == 'm') {
                                                            $g = "male";
                                                        } else {
                                                            $g = "female";
                                                        }
                                                        ?>
                                                        <td width="70%"><b><?php echo ucfirst($g); ?></b></td>  
                                                    </tr> 
                                                    <tr>  
                                                        <td width="30%">ContactNo</td>  
                                                        <td width="70%"><b><?php echo ucfirst($row['contactno']); ?></b></td>  
                                                    </tr> 
                                                    <tr>  
                                                        <td width="30%">E-mail</td>  
                                                        <td width="70%"><b><?php echo $row['email']; ?></b></td>  
                                                    </tr> 
                                                    <tr>  
                                                        <td width="30%">Address</td>  
                                                        <td width="70%"><b><?php echo $row['address']; ?></b></td>  
                                                    </tr> 


                                                </table>
                                            </div>
                                        </div>

                                        <!-- Modal footer -->
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <?php
                        }
                    }
                    ?>





                    <div class="content">
                        <div class="animated fadeIn">
                            <div class="row">

                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <strong class="card-title"><b style="color: #0087ba;">Customer Requirements</b></strong>
                                        </div>
                                        <div class="card-body">
                                            <table id="bootstrap-data-table" style="width: 100px !important; " class="table table-hover table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>Image</th>
                                                        <th>Name</th>
                                                        <th>Fineness</th>
                                                        <th>Type</th>
                                                        <th>Category</th>
                                                        <th>Quantity</th>
                                                        <th>Weight</th>
                                                        <th>Price</th>
                                                         <th>Date</th>
                                                        <th>Status</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    if ($av == 1) {
                                                        echo $dataview;
                                                    } else {
                                                        echo $dataview;
                                                    }
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div><!-- .animated -->
                    </div><!-- .content -->
                </section>


            </div>



            <div class="clearfix"></div>

            <?php
            include ('footer.php');
            ?>

        </div>
        <?php
        include('script.php');
        ?>
        <div id="container">



        </div>



    </body>

</html>