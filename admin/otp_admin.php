<?php
ob_start();

include_once './connection.php';

if(isset($_GET['em']))
{
    $em=$_GET['em'];
    
    base64_encode($em);
    echo $em;
    
    //exit();
}
?>


<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Otp Admin</title>
        <meta name="description" content="Ela Admin - HTML5 Admin Template">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!--   <link rel="apple-touch-icon" href="images/favicon.png"> -->
        <link rel="shortcut icon" href="images/Capture.png"> 

        <link rel="stylesheet" href="assets/css/normalize.css">
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/css/themify-icons.css">
        <link rel="stylesheet" href="assets/css/pe-icon-7-filled.css">
        <link rel="stylesheet" href="assets/css/flag-icon.min.css">
        <link rel="stylesheet" href="assets/css/cs-skin-elastic.css">
        <!-- <link rel="stylesheet" href="assets/css/bootstrap-select.less"> -->
        <link rel="stylesheet" href="assets/css/style.css">

        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

    <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script> -->

    </head>
    <body class="bg-light">


        <div class="sufee-login d-flex align-content-center flex-wrap">
            <div class="container">
                <div class="login-content">
                    <div class="login-logo">
                        <a href="#">
                            <img class="align-content" src="images/logo11.png" alt="">
                        </a>
                    </div>
                    <div class="login-form">
                        <form action="#" method="post">
                            <p>Otp is sent to your Registered E-mail </p>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-envelope"></i></div>
                                    <input type="password" id="txtotp" name="txtotp" placeholder="Enter OTP here" class="form-control" required="">
                                </div>
                            </div>

                            <input type="submit" name="btnotp" value="Submit" class="btn btn-secondary btn-flat m-b-15">
                            <center>
                                <?php
                                if (isset($_GET['hid'])) {
                                    $dopt = base64_decode($_GET['hid']);
                                    echo $dopt;

                                    if (isset($_POST['btnotp'])) {
                                        if ($dopt == $_POST['txtotp']) {
                                            header("Location:Change_forgot_admin.php?em1=". base64_encode($em)."");
                                            echo "$dopt";
                                        } else {
                                            ?>
                                <p style="color: red;">Otp Not Match</p>
                                            <?php
                                        }
                                    }
                                }
                                ?>
                            </center>

                        </form>
                    </div>
                </div>
            </div>
        </div>

        <?php
        include "footer.php";
        ?>


        <script src="assets/js/vendor/jquery-2.1.4.min.js"></script>
        <script src="assets/js/popper.min.js"></script>
        <script src="assets/js/plugins.js"></script>
        <script src="assets/js/main.js"></script>


    </body>
</html>
