
<html>
    <body>
<footer class="site-footer">
            <div class="footer-inner bg-white">
                <div class="row">
                    <div class="col-sm-6">
                        Copyright &copy; <?php echo date("Y") ?> The Jewellary
                    </div>
                    <div class="col-sm-6 text-right">
                        Designed by<a href="#"> <strong>Simex ltd.</strong></a> 
                    </div>
                </div>
            </div>
        </footer>
        </body>
</html>