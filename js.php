
<!--===============================================================================================-->	
<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
<script src="vendor/bootstrap/js/popper.js"></script>
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="vendor/select2/select2.min.js"></script>
<script>
    $(".js-select2").each(function () {
        $(this).select2({
            minimumResultsForSearch: 20,
            dropdownParent: $(this).next('.dropDownSelect2')
        });
    })
</script>
<!--===============================================================================================-->
<script src="vendor/daterangepicker/moment.min.js"></script>
<script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
<script src="vendor/slick/slick.min.js"></script>
<script src="js/slick-custom.js"></script>
<!--===============================================================================================-->
<script src="vendor/parallax100/parallax100.js"></script>
<script>
    $('.parallax100').parallax100();
</script>
<!--===============================================================================================-->
<script src="vendor/MagnificPopup/jquery.magnific-popup.min.js"></script>
<script>
    $('.gallery-lb').each(function () { // the containers for all your galleries
        $(this).magnificPopup({
            delegate: 'a', // the selector for gallery item
            type: 'image',
            gallery: {
                enabled: true
            },
            mainClass: 'mfp-fade'
        });
    });
</script>
<!--===============================================================================================-->
<script src="vendor/isotope/isotope.pkgd.min.js"></script>
<!--===============================================================================================-->
<script src="vendor/sweetalert/sweetalert.min.js"></script>
<script>
    $('.js-addwish-b2').on('click', function (e) {
        e.preventDefault();
    });

    $('.js-addwish-b2').each(function () {
        var nameProduct = $(this).parent().parent().find('.js-name-b2').html();
        $(this).on('click', function () {
            swal(nameProduct, "is added to wishlist !", "success");

            $(this).addClass('js-addedwish-b2');
            $(this).off('click');
        });
    });

    $('.js-addwish-detail').each(function () {
        var nameProduct = $(this).parent().parent().parent().find('.js-name-detail').html();

        $(this).on('click', function () {
            swal(nameProduct, "is added to wishlist !", "success");

            $(this).addClass('js-addedwish-detail');
            $(this).off('click');
        });
    });

    /*---------------------------------------------*/

    $('.js-addcart-detail').each(function () {
        var nameProduct = $(this).parent().parent().parent().parent().find('.js-name-detail').html();
        $(this).on('click', function () {
            swal(nameProduct, "is added to cart !", "success");
        });
    });
</script>
<!--===============================================================================================-->
<script src="vendor/perfect-scrollbar/perfect-scrollbar.min.js"></script>
<script>
    $('.js-pscroll').each(function () {
        $(this).css('position', 'relative');
        $(this).css('overflow', 'hidden');
        var ps = new PerfectScrollbar(this, {
            wheelSpeed: 1,
            scrollingThreshold: 1000,
            wheelPropagation: false,
        });

        $(window).on('resize', function () {
            ps.update();
        })
    });
</script>
<!--===============================================================================================-->
<script src="js/main.js"></script>
<script src="slick.min.js">
</script>

<!-- FOR MODEL ACCOUNT -->

<script>
    function profile(id)
    {
      //  alert($("#mname1").val());

       // alert($("#fname1").val() + $("#mname1").val() + $("#lname1").val() + $("#address1").val() + $("#city1").val() + $("#g11:checked").val() + $("#contact1").val());

        //console.log($("#mname1").val());



       // window.alert(id);
        $.ajax({

            url: "ajaxfile.php",
            type: "post",
            data: {profile_id: id, fname: $('#fname1').val(), mname: $('#mname1').val(), lname: $('#lname1').val(), address: $('#address1').val(), gender: $('#g11:checked').val(), city: $('#city1').val(), email: $("#email").val(), contact: $('#contact1').val()},
            success: function (result) {

                alert("Profile Update Successfully");
                // console.log(result);
                if (result == 'success')
                {
                    alert("Profile Update Successfully");
                }
            }
        });

    }
</script>
<!-- END MODEL ACCOUNT -->


<!-- FOR MODEL CHANGE PASSWORD -->

<script>
    function change(id)
    {

        // window.alert(id);

        $.ajax({
            url: "ajaxfile.php",
            type: "post",
            data: {change_id: id, current: $('#current').val()},
            success: function (result)
            {
               // alert(result);
                $("#dv").html(result);

            }
        });

    }



    function change1(id)
    {

        if ($("#current").val() == "" || $("#password").val() == "" || $("#confirm_password").val() == "")
        {
            $("#dv").html("Please Fill All Field");
        } else
        {

           // window.alert(id);

            $.ajax({
                url: "ajaxfile.php",
                type: "post",
                data: {change_id1: id, current: $('#current').val(), password: $('#password').val(), cpassword: $('#confirm_password').val()},
                success: function (result)
                {
                    alert(result);
                   // alert("Password Change Successfully");
                    //$("#dv").html(result);

                }
            });
        }
    }
</script>

<!--END CHANGE PASSWORD MODEL -->


<!-- Add To Cart-->

<script>


    function plusquant(id)
    {
        // alert("sdfghj");

        var qu = $("#quant_" + id).val();
        //  if(qu < 5)
        // {
        qu++;
        // }
        $("#quant_" + id).val(qu);
        //alert(qu);
    }

    function minusquant(id)
    {
        // alert("sdfghj");

        var qu = $("#quant_" + id).val();
        if (qu > 1)
        {
            qu--;
        }
        //alert(qu);

        $("#quant_" + id).val(qu);
    }


    // $(document).ready(function(){
    function myfunction(id)
    {


        var tq = $("#hd1_" + id).val();
        var gq = $("#quant_" + id).val();

       // alert("table:" + tq);
       // alert("get:" + gq);

        if (parseInt(gq) > parseInt(tq))
        {
            alert("Quantity Not Exist!!");
        } else
        {
            //alert(id);
            $.ajax({
                url: "ajaxfile.php",
                type: "post",
                data: {cartid: id, quantity: $("#quant_" + id).val()},
                success: function (result)
                {
                   // alert(result);

                }
            });
            //alert("Exist!!");
        }

    }
    // });
</script>
<!--  End Add To cart-->

<!--Remove Product in Cart -->

<script>

    function rcart(id)
    {
       // alert("product_id : " + id);
        $.ajax({
            url: "ajaxfile.php",
            type: "post",
            data: {rid: id},
            success: function (result)
            {
       //         alert("For Ajax : " + result);
       //         alert('#remove_' + id);

                if (confirm("Are you sure want to Delete!")) {
                    true;
                    if (result = "Success")
                    {
         //               alert("Bhargav Patel");
                        $("#remove_" + id).hide('slow');
                    }
                } else
                {
                    false;
                }
            }
        });


    }

</script>

<!--End Remove Product in Cart -->


<!--Add Product in Cart-->
<script>
    function cartadd() {
      //  alert("Bhargav");
        $.ajax({
            url: "ajaxfile.php",
            type: "post",
            data: {addcart: 1},
            success: function (result)
            {
               // alert(result);
                $("#add1").html(result);

            }
        });
    }
    
</script>
<!--End Add Product in Cart-->


<!-- Show Category-->
<script>
    function category(id)
    {
        alert(id);
        
         $.ajax({
            url: "ajaxfile.php",
            type: "post",
            data: {category_id: id},
            success: function (result)
            {
                console.log(result);
                alert(result);
              $("#cate1").html(result);

            }
        });
        
    }
</script>
<!--End Show Category -->
