<?php
ob_start();
include 'Connection.php';
?>

<!-- Header -->
<header class="header-v3">
    <!-- Header desktop -->
    <div class="container-menu-desktop trans-03">
        <div class="wrap-menu-desktop" style="background-color:">
            <nav class="limiter-menu-desktop p-l-45">

                <!-- Logo desktop -->		
                <a href="Home_Customer.php" class="logo">
                    <img src="images/icons/logo-02.png" alt="IMG-LOGO">
                </a>
                <!-- Menu desktop -->
                <div class="menu-desktop">
                    <ul class="main-menu">
                        <li>
                            <a href="Home_Customer.php">Home</a>
                            <!--<ul class="sub-menu">
                                    <li><a href="index.html">Homepage 1</a></li>
                                    <li><a href="home-02.html">Homepage 2</a></li>
                                    <li><a href="home-03.html">Homepage 3</a></li>
                            </ul>-->
                        </li>

                        <li>
                            <!-- <a href="product.html">Shop</a> -->
                            <a href="new_product.php">New Product</a>
                        </li>



                        <li>
                            <!--<a href="about.html">About</a>-->
                            <a href="about.php">About</a>
                        </li>

                        <li>
                            <!--<a href="contact.html">Contact</a> -->
                            <a href="Contactus.php">Contact</a>
                        </li>
                    </ul>
                </div>






                <!-- Icon header -->
                <div class="wrap-icon-header flex-w flex-r-m h-full">			

                    <?php
                    if (isset($_SESSION['suname']) and $_SESSION['spass']) {
                        ?>
                        <div class="flex-c-m h-full p-r-25 bor6">
                            <div class="cl0  trans-04 p-lr-11 icon-header-noti data-notify-2">

                                <?php
                                // echo $_SESSION['suname'];
                                // echo "<br>" . $_SESSION['spass'];




                                //$servername = "localhost";
                                //$username = "root";
                                //$password = "";
                               // $dbname = "jewellary1";

                                $u = $_SESSION['suname'];
                                $p = $_SESSION['spass'];

                                $sql = "SELECT * FROM tbl_customer ";

                                $result = $conn->query($sql);

                                if ($result->num_rows > 0) {

                                    while ($row = $result->fetch_assoc()) {



                                        if ($row["password"] == $p and $row["email"] == $u and $row["status"] == "1") {


                                            echo "Welcome : <strong>" . $row['fname'] . "</strong>";
                                        } else {
                                            
                                        }
                                    }
                                } else {
                                    echo "0 results";
                                }

                                $conn->close();
                                ?>




                            </div>
                        </div>


                        <div class="flex-c-m h-full p-r-25 bor6">
                            <div class="cl0 hov-cl1 trans-04 p-lr-11 icon-header-noti data-notify-2">
                                <a  href="logout.php" class="ml-auto" style="font-size:14px; font-family: Poppins-Medium; color:white;">Log out</a>
                            </div>
                        </div>
                        <?php
                    } else {
                        ?>

                        <div class="flex-c-m h-full p-r-25 bor6">
                            <div class="cl0 hov-cl1 trans-04 p-lr-11 icon-header-noti data-notify-2">
                                <a  href="Login1.php" class="ml-auto" style="font-size:14px; font-family: Poppins-Medium; color:white;">Login </a>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="flex-c-m h-full p-r-25 bor6">
                        <div class="icon-header-item cl0 hov-cl1 trans-04 p-lr-11 icon-header-noti js-show-cart" data-notify="2">
                            <!-- Cart Symbol -->     <a href="javascript:void(0);" style="color: white" onclick="cartadd()">  <i class="zmdi zmdi-shopping-cart"></i></a>
                        </div>
                    </div>


                    <div class="flex-c-m h-full p-lr-19">
                        <div class="icon-header-item cl0 hov-cl1 trans-04 p-lr-11 js-show-sidebar">
                            <i class="zmdi zmdi-menu"></i>
                        </div>
                    </div>
                </div>
            </nav>
        </div>	
    </div>

   
    
    
    
    
    <!-- Header Mobile -->
    <div class="wrap-header-mobile">
        <!-- Logo moblie -->		
        <div class="logo-mobile">
            <a href="Home_Customer.php"><img src="images/icons/logo-01.png" alt="IMG-LOGO"></a>
        </div>

        <!-- Icon header -->


        <div class="wrap-icon-header flex-w flex-r-m h-full m-r-15">
            <div class="flex-c-m h-full p-r-5">
                <div class="icon-header-item cl2 hov-cl1 trans-04 p-lr-11 icon-header-noti js-show-cart" data-notify="2" onclick="r1();">
                  <!--CART -->  <i class="zmdi zmdi-shopping-cart"></i>
                </div>
            </div>
        </div>

        <!-- Button show menu -->
        <div class="btn-show-menu-mobile hamburger hamburger--squeeze">
            <span class="hamburger-box">
                <span class="hamburger-inner"></span>
            </span>
        </div>
    </div>


    <!-- Menu Mobile -->
    <div class="menu-mobile">
        <ul class="main-menu-m">
            <li>
                <a href="Home_Customer.php">Home</a>

            </li>

            <li>
                <a href="new_product.php">New Product</a>
            </li>

            <li>
                <a href="about.php">About</a>
            </li>

            <li>
                <a href=Contactus.php>Contact</a>
            </li>


            <?php if (isset($_SESSION['suname']) and $_SESSION['spass']) { ?>

                <li>
                    <a href="logout.php">Logout</a>
                </li>


                <li>
                    <a href="#" data-toggle="modal" data-target="#myModal1">
                        My Account
                    </a>
                </li>

                <li>
                    <a href="#">
                        Change password
                    </a>
                </li>

                <li>
                    <a href="#">
                        Upload Your Requriment Product
                    </a>
                </li>

            <?php } else { ?>

                <li>
                    <a href="Login1.php">Login</a>
                </li>

            <?php } ?>

        </ul>
    </div>

    <!-- Modal Search -->
    <div class="modal-search-header flex-c-m trans-04 js-hide-modal-search">
        <button class="flex-c-m btn-hide-modal-search trans-04">
            <i class="zmdi zmdi-close"></i>
        </button>

        <form class="container-search-header">
            <div class="wrap-search-header">
                <input class="plh0" type="text" name="search" placeholder="Search...">

                <button class="flex-c-m trans-04">
                    <i class="zmdi zmdi-search"></i>
                </button>
            </div>
        </form>
    </div>

    <!-- MODEL FOR MYACCOUNt-->

    <div class="modal fade" id="myModal1">
        <div class="modal-dialog modal-dialog-centered modal-md ">
            <div class="modal-content">

                <!-- Modal Header -->

                <div class="modal-header">
                    <h4 class="modal-title ">My Profile</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>


                <!-- Modal body -->
                <div class=" js-hide-sidebar"></div>
                <div class="modal-body">

                    <?php
                    if (isset($_SESSION['suname']) and $_SESSION['spass']) {

                        $p = $_SESSION['spass'];
                        $u = $_SESSION['suname'];
                       // $conn = new mysqli('Localhost', 'root', '', 'jewellary1');
                        $sql = "SELECT * FROM tbl_customer where password='$p' && email='$u'";

                        $result = mysqli_query($conn, $sql);

                        //$result = $conn->query($sql);

                        if (mysqli_num_rows($result)) {

                            while ($row = mysqli_fetch_assoc($result)) {

                                //  echo $row['city_id'];
                                ?>
                                <form action="#" method="post" id="form1">

                                    <div class="login-form">

                                        <div class="row form-group">
                                            <div class="col col-md-12">
                                                <div class="input-group">
                                                    <input type="text" id="fname" name="txtfname" class="form-control" placeholder="First Name" required pattern="^[A-Za-z]{3,50}$" title="Please Enter Valid Name" value="<?php echo $row['fname'] ?>">
                                                    &nbsp;<input type="text" id="mname" name="txtmname" class="form-control" placeholder="Middle Name" required pattern="^[A-Za-z]{3,50}$" title="Please Enter Valid Name" value="<?php echo $row['mname'] ?>">
                                                    &nbsp;<input type="text" id="lname" name="txtlname" minlength="3" class="form-control" placeholder="Last Name" required  pattern="^[A-Za-z]{3,50}$" title="Please Enter Valid Name" value="<?php echo $row['lname'] ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <textarea type="text" id="address" name="txtaddress" placeholder="Address" class="form-control" required pattern="^[A-Za-z]{3,150}$" title="Please Enter Valid Address"><?php echo $row['address'] ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">


                                                <select name="txtcity"  class="form-control" id="city" >

                                                    <?php
                                                    $sql1 = "select * from tbl_city";
                                                    $res1 = mysqli_query($conn, $sql1);



                                                    if (mysqli_num_rows($res1)) {
                                                        while ($row1 = mysqli_fetch_assoc($res1)) {

                                                            if ($row['city_id'] == $row1['city_id']) {

                                                                $se = "selected";
                                                            } else {
                                                                $se = "";
                                                            }
                                                            ?>

                                                            <option value="<?php echo $row1['city_id'] ?>" <?php echo $se ?>><?php echo $row1['city_name'] ?></option> 

                                                            <?php
                                                        }
                                                    } else {
                                                        echo "Data not Found";
                                                    }
                                                    ?>

                                                </select>


                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">

                                                <?php
                                                //  echo "Bhargav :".$row['gender'];
                                                if ($row['gender'] == 'm') {
                                                    $m = "checked";
                                                    $f = "";
                                                } else {
                                                    $m = "";
                                                    $f = "checked";
                                                }
                                                ?>
                                                Gender :&nbsp;<input type="radio" name="gender" id="g1"  value="m" <?php echo $m ?>  >&nbsp;Male&nbsp;
                                                &nbsp;<input type="radio" name="gender" id="g1" value="f" <?php echo $f ?>>&nbsp;Female

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" id="contact" name="txtcontact" placeholder="Contact number" class="form-control" maxlength="10" minlength="10" onkeypress='return event.charCode >= 48 && event.charCode <= 57' pattern="[7986][0-9]{9}" title="Please Enter Valid Mobile Number" required value="<?php echo $row['contactno'] ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="email" id="email" disabled name="txtemail" placeholder="Email" class="form-control" required pattern="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" title="Please Enter Valid Email Address" value="<?php echo $row['email'] ?>">
                                            </div>
                                        </div>

                                        <center > <input type="button" id="profile1" class="flex-c-m stext-101 cl0 size-121 bg3 bor1 hov-btn3 p-lr-15 trans-04 pointer " data-dismiss="modal"  style="width: 60%;" name="update" value="Update Information" onclick="profile(<?php echo $row['customer_id'] ?>)" ></center>

                                    </div>
                                </form>
                                <?php
                            }
                        } else {
                            echo "0 results";
                        }

                        $conn->close();
                    }



                    if (isset($_GET['update'])) {
                        echo "Bhargav";
                    }
                    ?>
                </div>



            </div>
        </div>
    </div>


    <!-- END MODEL MYACCOUNt-->