<?php
session_start();
ob_start();

if (isset($_SESSION['suname'])) {
    
} else {
    header('Location:Home_Customer.php');
}
?>
<html lang="en">
    <head>
        <title>Home</title>
<?php include('Head.php') ?>

    </head>
    <style>
        .header-v3 .wrap-menu-desktop{
            background-color: black;
        }
        h6.gt{
            text-align: center;


            position: relative;
        }

        h6
        {
            color: #000000;
            font-size: 20px;

            font-weight: 400;
            display: block;
            padding-bottom: 10px;
        }

        .before{content:'';
                background:url(home_line.png) no-repeat center;
                position:absolute;left:0;
                right:0;bottom:0;
                margin-bottom:-10px;
                width:100%;
                height:13px;
        }
    </style>
    <body class="animsition">

        <header class="header-v3">
        <?php include('Header.php') ?>
        </header>
<?php //include('Cart.php')  ?>

        <!--    <br><br><br><br><br><br><br><br><br><br><br><br><br>-->


        <div class="container-fluid"><br>

            <?php
            include('Cart.php');
            include './Connection.php';
            // $conn = new mysqli('Localhost', 'root', '', 'jewellary1');

            $s1 = $_SESSION['suname'];
            $p1 = $_SESSION['spass'];

            // echo $s1 . $p1;
            $sql3 = "select * from tbl_customer where email='$s1' && password='$p1'";
            $res3 = mysqli_query($conn, $sql3);

            if (mysqli_num_rows($res3)) {
                //echo "Record Found";
                $row3 = mysqli_fetch_assoc($res3);

                $cid = $row3['customer_id'];

                // echo $cid;
            }
            ?>

            <form class="bg0 p-t-75 p-b-85" action="payment.php" method="post">
                <div class="">
                    <h6 class="gt text-success">
                        <b>Confiorm Your Order And Address</b>
                        <div class="before"></div>

                    </h6><br>
                    <div class="row">
                        <div class="col-lg-10 col-xl-8 m-lr-auto m-b-50">
                            <div class="m-l-25 m-r--38 m-lr-0-xl">
                                <div class="wrap-table-shopping-cart">

                                    <table class="table-shopping-cart">

                                        <tr class="table_head">
                                            <th class="column-1">Product</th>
                                            <th class="column-2"></th>
                                            <th class="column-3">Price</th>
                                            <th class="column-4">Quantity</th>
                                            <th class="column-5">Total</th>

                                        </tr>


                                        <?php
                                        $sql2 = "select tbl_product.*,tbl_pcategory.name,tbl_material_type.type,tbl_cart.product_id,tbl_cart.Quantity,tbl_cart.total_amount from tbl_product INNER JOIN tbl_pcategory_type on tbl_product.pct_id=tbl_pcategory_type.cat_type_id INNER join tbl_pcategory on tbl_pcategory_type.cat_id=tbl_pcategory.category_id INNER join tbl_material_type on tbl_pcategory_type.matel_id=tbl_material_type.mtype_id INNER join tbl_cart  on  tbl_cart.product_id=tbl_product.model_no where tbl_cart.customer_id='$cid'";
                                        $res2 = mysqli_query($conn, $sql2);

                                        if (mysqli_num_rows($res2)) {
                                            $sum = 0;
                                            while ($row2 = mysqli_fetch_assoc($res2)) {
                                                ?>

                                                <tr class="table_row" id="<?php echo "remove_" . $row2['product_id'] ?>">
                                                    <td class="column-1" >
                                                        <div class="how-itemcart1" onclick="rcart('<?php echo $row2['product_id'] ?>')">
                                                            <img src="<?php echo "admin/" . $row2['image'] ?>" alt="IMG" >
                                                        </div>
                                                    </td>
                                                    <td class="column-2"><?php echo "<b class='text-success'>" . ucwords($row2['type']) . "</b> " . ucwords($row2['name']) ?><br><?php echo "Model No : " . $row2['product_id']; ?></td>
                                                    <td class="column-3"><i class="fa fa-rupee"></i>&nbsp;<?php echo $row2['price'] ?></td>
                                                    <td class="column-4">
                                                        <div class="wrap-num-product flex-w m-l-auto m-r-0">


                                                            <center>   <input class="mtext-104 cl3 txt-center num-product" type="number" name="num-product1" disabled value="<?php echo $row2['Quantity'] ?>" id="quant_<?php echo $row2['product_id'] ?>"></center>


                                                        </div>
                                                        <p id="error"></p>
                                                        <input type="hidden" name="tablequantity" id="tableq_<?php echo $row2['model_no'] ?>" value="<?php echo $row2['quantity'] ?>">
                                                    </td>
                                                    <td class="column-5">  <?php
                                                        $tam = $row2['price'] * $row2['Quantity'];

                                                        $up = $row2['product_id'];
                                                        echo "<p id='updatep_$up'><i class='fa fa-rupee'></i>" . $tam . "</p>";



                                                        $sum = $sum + $tam;
                                                        ?></td>



                                                </tr>


                                                <?php
                                            }
                                        } else {
                                            echo "Record Not Found";
                                        }
                                        ?>


                                    </table>
                                </div>

                                <div class="flex-w flex-sb-m bor15 p-t-18 p-b-15 p-lr-40 p-lr-15-sm">
                                    <!-- <div class="flex-w flex-m m-r-20 m-tb-5">
                                         <input class="stext-104 cl2 plh4 size-117 bor13 p-lr-20 m-r-10 m-tb-5" type="text" name="coupon" placeholder="Coupon Code">
     
                                         <div class="flex-c-m stext-101 cl2 size-118 bg8 bor13 hov-btn3 p-lr-15 trans-04 pointer m-tb-5">
                                             Apply coupon
                                         </div>
                                     </div> -->

                                    <!-- <div class="flex-c-m stext-101 c12 size-119 bg8 bor13 hov-btn3 p-lr-15 trans-04 pointer m-tb-10">
                                         Update Cart
                                     </div> -->

                                    <div class="size-208">
                                        <span class="mtext-101 cl2 ">
                                            Total Amount&nbsp;&nbsp;:&nbsp; &nbsp;<b><p id="dis" class="text-success"><i  class="fa fa-rupee"></i><?php echo " " . $sum ?></p></b>
                                        </span>
                                    </div>
                                <!--<a href="payment.php" class="flex-c-m stext-101 cl0 size-119  bg3 bor14 hov-btn3 p-lr-15 trans-04 pointer">
                                        Pleace Order
                                    </a>-->
                                
                                <input type="submit" class="flex-c-m stext-101 cl0 size-119  bg3 bor14 hov-btn3 p-lr-15 trans-04 pointer" name="please_order" value="Please Order">
                                    <!--Pleace Order
                                    </a>-->


                                </div>
                            </div>
                        </div>

                        <div class="col-sm-10 col-lg-7 col-xl-4 m-lr-auto m-b-50">
                            <div class="bor10 p-lr-40 p-t-30 p-b-40 m-l-63 m-r-40 m-lr-0-xl p-lr-15-sm">
                                <h4 class="text-109 cl3 p-b-30">
                                    Total Amount
                                </h4>

                                <div class="flex-w flex-t bor12 p-b-13">
                                    <div class="size-208">
                                        <span class="stext-110 cl2">
                                            Subtotal:
                                        </span>
                                    </div>

                                    <div class="size-209 ">
                                        <span class="mtext-110 cl2 text-success">
                                            &nbsp; <b><p id="dis1"><i class="fa fa-rupee"></i>&nbsp;<?php echo $sum ?></p></b>
                                        </span>
                                    </div>
                                </div>

                                <div class="flex-w flex-t bor12 p-t-15 p-b-30">
                                    <div class="size-208 w-full-ssm">
                                        <span class="stext-110 cl2">
                                           
                                            Address: &nbsp;<input type="button" class="form-group bg-success" name="showshipping" onclick="shows()" value="Change" style="width: 80%;color:white;">
                                        </span>
                                    </div>

                                    <div class="size-209 p-r-18 p-r-0-sm w-full-ssm" >
                                         <div class="stext-110 cl6 p-t-2 text-success" id="faddress" >
                                                <br><?php
                                                include './Connection.php';
                                                //echo $_SESSION['suname'];
                                                $e1 = $_SESSION['suname'];

                                                $sql5 = "select * from tbl_customer where email='$e1'";
                                                $res5 = mysqli_query($conn, $sql5);

                                                $row5 = mysqli_fetch_assoc($res5);

                                                echo $row5['address'];

                                                $cno = $row5['contactno'];
                                                ?>
                                            </div>

                                        <div class="stext-110 cl6 p-t-2 text-success" id="saddress" >
                                        <div class="form-group">
                                            <div class="input-group">
                                                <textarea class="form-group" name="shipping_address" style="border :1px solid #e6e6e6;height: 10%;"></textarea>
                                                
                                            </div>
                                        </div>
                                        </div>

                                    </div>



                                    <div class="size-208 w-full-ssm">
                                        <span class="stext-110 cl2">
                                            Contactno:
                                        </span>
                                    </div>
                                    <div class="size-209 p-r-18 p-r-0-sm w-full-ssm">
                                        <b> <p class="stext-110 cl6 p-t-2 text-success" >

                                                <br>+91 <?php echo $cno ?>

                                            </p></b></div>
                                    
                                    <div class="size-208 w-full-ssm" style="width: 100%">
                                        <span class="stext-110 cl2">
                                           Alternative Contactno:
<!--                                           <input type="button" class="form-group bg-success" name="showshipping" onclick="shows()" value="Alternative Contactno" style="width: 80%;color:white;">-->
                                        </span>
                                    </div>
                                    <div class="size-209 p-r-18 p-r-0-sm w-full-ssm" style="width: 100%">
                                        <b> <p class="stext-110 cl6 p-t-2 text-success" >

                                                <input type="text" id="acontact" name="altcontact" placeholder="Contact no" class="form-control" maxlength="10" minlength="10" onkeypress='return event.charCode >= 48 && event.charCode <= 57' pattern="[7986][0-9]{9}" title="Please Enter Valid Mobile Number" >

                                            </p></b></div>
                                </div>

                                <div class="flex-w flex-t p-t-27 p-b-33">
                                    <div class="size-208">
                                        <span class="mtext-101 cl2">
                                            Total:
                                        </span>
                                    </div>

                                    <div class="size-209 p-t-1">
                                        <span class="mtext-110 cl2 text-success">
                                            <b><p id="dis2"><i  class="fa fa-rupee"></i> <?php echo $sum ?></p></b>
                                        </span>
                                    </div>
                                </div>

                                <!--<button class="flex-c-m stext-101 cl0 size-116 bg3 bor14 hov-btn3 p-lr-15 trans-04 pointer">
                                     Checkout
                                 </button>-->
                            </div>
                        </div> 
                    </div>
                </div>
            </form>


        </div>



        <footer class="bg3 p-t-75 p-b-32">
<?php include('Footer.php') ?>
        </footer>
            <?php include('js.php') ?>
    </body>
</html>

<script>
   // document.getElementById("saddress").style.visibility="hidden";
   // document.getElementById("saddress").style.display = 'none';
   // $("#saddress").hide();
   $("#saddress").hide();
    function shows()
    {
       // alert("Hii");
        $("#faddress").hide();
        $("#saddress").show();
        
    }
</script>

<?php

if(isset($_POST['please_order']))
{
    echo $_POST['shipping_address'];
    echo $_POST['altcontact'];
    
    $_SESSION['saddress']=$_POST['shipping_address'];
    $_SESSION['acontactno']=$_POST['altcontact'];
    
    echo $_SESSION['saddress'];
    echo $_SESSION['acontactno'];
   // header('location : payment.php');
}

?>