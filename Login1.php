<?php
session_start();
include './Connection.php';

if (isset($_SESSION['suname'])) {
    header('Location:Home_Customer.php');
} else {
    
}
?>
<html lang="en">
    <head>
        <title>Login</title>
        <?php include('Head.php') ?>

    </head>
    <style> 
        .bg1{

            background:url(bg.jpg); background-size: cover;  
        }
    </style>
    <body class="animsition">

        <header class="header-v3">
            <?php include('Header.php') ?>
        </header>
        <?php include('Cart.php') ?>
        <div style="clear:both;"></div>



        <div  class="bg1" style="height:100%;" >


            <div class="container">
                <div class="row">
                    <div class="col-sm-7"></div>

                    <div class="col-sm-5" style="position: relative; top: 160px;">
                        <form action="#" method="post">
                            <div class="login-form">
                                <h4 class="mtext-105 cl2 txt-center p-b-30" style="font-size: 40px; color:black; margin-bottom: 0px; padding: 13px 0;">
                                    <b><u>Login</u></b>
                                </h4>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-user"></i></div>
                                        <input type="email" name="username" placeholder="Enter Username" class="form-control" required >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-lock"></i></div>
                                        <input type="password" name="password" placeholder="Enter Password" class="form-control" required>
                                    </div>
                                </div>



                                <?php
                                if (isset($_POST['login'])) {
                                   // $servername = "localhost";
                                    //$username = "root";
                                    //$password = "";
                                    //$dbname = "jewellary1";

                                    $susername = $_POST['username'];
                                    $spassword = $_POST['password'];
                                    
                                    $pw3= md5($_POST['password']);

                                    //$conn = new mysqli($servername, $username, $password, $dbname);

                                    if ($conn->connect_error) {
                                        die("Connection failed: " . $conn->connect_error);
                                    }
                                    $sql = "SELECT * FROM tbl_login ";

                                    $result = $conn->query($sql);

                                    if ($result->num_rows > 0) {

                                        while ($row = $result->fetch_assoc()) {
                                            if ($row["password"] == md5($_POST['password']) and $row["username"] == $_POST['username'] and $row["status"] == "1") {
                                                $_SESSION['suname'] = $_POST['username'];
                                                $_SESSION['spass'] = $pw3;
                                                header('Location:Home_Customer.php');
                                            } else {
                                                $x = 1;
                                            }
                                        }
                                        if ($x == 1) {
                                            ?>
                                            
                                            <center style="color:red"> Please Enter Valid Username or Password</center>
                                            <?php
                                        }
                                    } else {
                                        echo "0 results";
                                    }

                                    $conn->close();
                                }
                                ?>


                                <table>
                                    <tr>
                                        <td>
                                            <div class="checkbox"><br><input type="checkbox" name="rememberme"></div>
                                        </td>
                                        <td>
                                            <br>&nbsp; Remember Me
                                            </div>
                                        </td>
                                        <td style="width:150px;">
                                        </td>
                                        <td class="ml-auto">
                                            <br><a href="forgot_admin" >Forgotten Password?</a>

                                        </td>
                                    </tr>
                                </table>

                                <br><input type="submit" class="flex-c-m stext-101 cl0 size-121 bg3 bor1 hov-btn3 p-lr-15 trans-04 pointer" style="width: 100%;" name="login" value="Sign in">
                                <div class="register-link m-t-15 text-center">
                                    <p>
                                        Register Here 
                                        <a href="register.php"> Sign Up Here</a>
                                    </p>
                                </div>
                            </div>
                        </form>



                    </div>
                </div>
            </div>




            <div  style="width:36%;  margin-left: 750px;  ">

            </div>
        </div>
        <footer class="bg3 p-t-75 p-b-32">
            <?php include('Footer.php') ?>
        </footer>
        <?php include('js.php') ?>
    </body>
</html>